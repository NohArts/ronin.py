from .helpers import *

##########################################################################

class PortMapper(dict):
    GENERATE = (7000,7999) # None

    def declare(self,targets):
        for alias,value in targets.iteritems():
            try:
                value = int(value)
            except:
                value = None

            if value is not None:
                self.assign(alias,value)

    def assign(self,alias,value=0):
        if type(value) in (int,long):
            value = self.normalize(value)

            if self.GENERATE is None:
                if value in self.values():
                    raise Exception("Duplicate port for '%s' : %d" % (alias,value))

            print "\t-> Assigned port '%s' to : %d" % (alias,value)

            self[alias] = value

        return value

    def normalize(self, value):
        if value <= 0:
            value = None

        rnd = random.Random()

        while (value is None) or value in self.values():
            value = rnd.randrange(*self.GENERATE)

        return value

##########################################################################

class WebRoute(Molecular):
    def initialize(self, *args, **kwargs):
        self._lst = {}

        self.trigger('prepare', *args, **kwargs)

    listing = property(lambda self: self._lst)

    def __getitem__(self, alias):
        return self._lst.get(alias,None)

    def depends(self, alias, version=None):
        if version in (None,False,0,'0'):
            version = '*'

        if alias in self._lst:
            if version!='*':
                self._lst[alias] = version
        else:
            self._lst[alias] = version

        return self

