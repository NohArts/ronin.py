from .helpers import *

##########################################################################

def cross_realm(key, **ctx):
    resp = dict(name=key, options={
        "uri_check":                    "strict",
        #"enable_meta_api":              True,
        #"bridge_meta_api":              False,
        #"event_dispatching_chunk_size": 100,
    })

    for x in ('store','roles'):
        resp[x] = load_json(spath('realm.d',key,x+'.json'))

    return resp

#*************************************************************************

def cross_worker(instance, **ctx):
    resp = {
        "type" : "guest",
        "executable" : instance['prog'],
        "arguments" : [str(x) for x in instance['args']],
        "options" : {
            "env" : {
                "vars" : dict([
                    (k,str(v))
                    for k,v in instance.get('vars', {}).iteritems()
                ]),
                "inherit" : True,
            },
            "workdir" : instance.get('work', '.')
        },
    }

    instance['dirs'] = instance.get('dirs', [])

    if len(instance['dirs']):
        resp['options']['watch'] = {
            "directories" : instance['dirs'],
            "action" : "restart"
        }

    return resp

##########################################################################

class Neurotic(Molecular):
    chip = property(lambda self: self.parent)
    name = property(lambda self: self.narrow)

    def initialize(self, *args, **kwargs):
        self.trigger('prepare', *args, **kwargs)

#*************************************************************************

class NeuroRealm(Neurotic):
    def prepare(self, *args, **kwargs):
        self._data = load_json(spath('realm.d',self.narrow,'store.json'))
        self._role = load_json(spath('realm.d',self.narrow,'roles.json'))

    store = property(lambda self: self._data)
    roles = property(lambda self: self._role)

    @property
    def __json__(self):
        return dict(name=key, options={
            "uri_check":                    "strict",
            #"enable_meta_api":              True,
            #"bridge_meta_api":              False,
            #"event_dispatching_chunk_size": 100,
        }, store=self.store, roles=self.roles)

#*************************************************************************

class NeuroSpecs(Neurotic):
    def prepare(self, *args, **kwargs):
        if not os.path.exists(self.conf_p):
            os.system('git clone git@bitbucket.org:%s/bushido.git %s' % (self.narrow,self.conf_p))

        if not os.path.exists(self.real_p):
            if os.path.exists(self.code_p):
                os.system('echo cp -afR %s %s' % (self.code_p,self.real_p))

        self.parent.log('DEBUG', 1, "-> Loading specs '%s' as : %s ...",self.narrow,self.alias)

        if os.path.exists(self.real_p):
            self._real = NeuroRealm(self.parent, self.nspace)

        for idx,sub in [
            #('ocean',['buckets.yaml']),
            #('cloud',['clouded.yaml']),
            #('daten',['dataset.yaml']),
        ]:
            pth = vpath(self.suffix+'-'+self.alias,'cfg',*sub)

            if os.path.exists(pth):
                self.parent.log('DEBUG', 2, "*) Config : %s",idx)

                cfg = load_yaml(pth)

                if idx=='ocean':
                    for dep in cfg:
                        if dep not in self.parent[idx]:
                            self.parent[idx][dep] = []

                        for row in cfg[dep]:
                            self.parent[idx][dep].append(row)
                elif idx=='cloud':
                    if idx not in self.parent:
                        self.parent[idx] = {}

                    if nrw not in self.parent[idx]:
                        self.parent[idx][nrw] = cfg
                else:
                    if idx not in self.parent:
                        self.parent[idx] = []

                    for row in cfg:
                        self.parent[idx].append(row)

        for lang in self.parent['langs']:
            pth = vpath(self.suffix+'-'+self.alias,'src',lang)

            if os.path.exists(pth):
                self.parent.log('DEBUG', 2, "#) Sources for '%s' at : %s",lang,pth)

                self.parent['langs'][lang].include(pth)

    realm = property(lambda self: self._real)
    alias = property(lambda self: os.environ.get('GHOST_%s' % self.narrow,''))

    suffix = property(lambda self: {
        'DOMAIN': 'dns',
        'NARROW': 'org',
        'PERSON': 'who',
    }.get(self.narrow,'vfs'))
    nspace = property(lambda self: 'ronin.%s.%s' % (self.suffix,self.alias))
    conf_p = property(lambda self: vpath(self.suffix+'-'+self.alias,'cfg'))
    real_p = property(lambda self: rpath('specs','realm.d',self.nspace))
    code_p = property(lambda self: vpath(self.suffix+'-'+self.alias,'cfg','wamp'))

#*************************************************************************

class NeuroAspect(Neurotic):
    def prepare(self, ns, *nss):
        self.parent.log('DEBUG', 2, "*) Aspect manifest : %s", self.narrow)

        pth = spath('aspect.d',self.narrow,'manifest.json')

        if os.path.exists(pth):
            try:
                meta = load_json(pth)
            except Exception,ex:
                pass

        self.parent.log('DEBUG', 2, "*) Aspect depends : %s", self.narrow)

        pth = spath('aspect.d',self.narrow,'depends.json')

        if os.path.exists(pth):
            try:
                cfg = load_json(pth)
            except Exception,ex:
                print pth,ex
                raise ex
                cfg = {}

            for nrw in self.parent['langs']:
                self.parent['langs'][nrw].enrichs(cfg.get(nrw, None))

        self.parent.log('DEBUG', 2, "*) Aspect routing : %s", self.narrow)

        pth = spath('aspect.d',self.narrow,'routing.json')

        if os.path.exists(pth):
            try:
                cfg = load_json(pth)
            except Exception,ex:
                print pth,ex
                raise ex
                cfg = {}

            if 'paths' in cfg:
                self.parent['paths'][self.narrow] = {
                    'type':  'path',
                    'paths': cfg['paths'],
                }

        self.parent.log('DEBUG', 2, "*) Aspect serving : %s", self.narrow)

        pth = spath('aspect.d',self.narrow,'serving.json')

        if os.path.exists(pth):
            try:
                cfg = load_json(pth)
            except Exception,ex:
                print pth,ex
                raise ex
                cfg = []

            for row in cfg:
                self.parent['serve'].append(row)

    def __call__(self, *args, **kwargs):
        ctx = self.parent

        pth = spath('aspect.d',self.narrow,'provision.py')

        print pth, spath('hello-world.py')

        if os.path.exists(pth):
            try:
                exec (load_text(pth), globals(), locals())
            except Exception,ex:
                print pth,ex

#*************************************************************************

class NeuroPlugin(Neurotic):
    def prepare(self, *args, **kwargs):
        self._meta = load_json(spath('kobudo','*',self.narrow,'manifest.json'))

    metadata = property(lambda self: self._meta)
    manifest = property(lambda self: self._meta)

    def __getitem__(self, key, default=None):
        if key in self._meta:
            return self._meta[key]

        return default

    verse = property(lambda self: self['verse'])
    alias = property(lambda self: self['alias'])
    usage = property(lambda self: self['usage'])

    disks = property(lambda self: self['disks'])
    ports = property(lambda self: self['ports'])
    links = property(lambda self: self['links'])

    routing = property(lambda self: self['routing'])
    working = property(lambda self: self['working'])
    depends = property(lambda self: self['depends'])

    @property
    def __json__(self):
        return dict(name=key, options={
            "uri_check":                    "strict",
            #"enable_meta_api":              True,
            #"bridge_meta_api":              False,
            #"event_dispatching_chunk_size": 100,
        }, store=self.store, roles=self.roles)

