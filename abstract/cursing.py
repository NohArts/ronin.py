from .library import *

##########################################################################

def has_keys(target, *fields):
    return reduce(operator.and_, [
        (key in target) for key in fields
    ] or [False])

#******************************************************************************

def singleton(handler):
    return handler()

################################################################################

bpath = os.path.abspath(os.curdir)
def rpath(*path): return os.path.abspath(os.path.join(bpath, *path))

akasha_path = os.environ.get('GHOST_AKASHA','%(HOME)s/codes/akasha' % os.environ)
def vpath(*path): return os.path.abspath(os.path.join(akasha_path, *path))

scroll_path = os.environ.get('GHOST_SCROLL',rpath('catalog'))
def cpath(*path): return os.path.abspath(os.path.join(scroll_path, *path))

ninshu_path = os.environ.get('GHOST_NINSHU',rpath('specs'))
def spath(*path): return os.path.abspath(os.path.join(ninshu_path, *path))

bushido_path = os.environ.get('GHOST_KOBUDO',rpath('..','bushido'))
def hpath(*path): return os.path.abspath(os.path.join(bushido_path, *path))

#*************************************************************************

def load_text(path): return open(path).read()

def write_text(data, path):
    with open(path,'w+') as f:
        f.write(data)

    return data

#*************************************************************************

def load_yaml(path): return yaml.load(load_text(path))

#*************************************************************************

def load_json(path): return json.loads(load_text(path))
def write_json(data, path): return write_text(json.dumps(data), path)

##########################################################################

#from fs import open_fs
#from fs.mountfs import MountFS
#from fs.multifs import MultiFS

#*************************************************************************

def fs_compose(cls, mapping):
    resp = None

    if type(mapping) in (tuple,set,frozenset,list):
        resp = MultiFS()

        for target in mapping:
            resp.addfs(target)

    elif hasattr(mapping, 'iteritems'):
        resp = MountFS()

        for prefix,target in mapping.iteritems():
            resp.mountdir(prefix, target)

    return resp

