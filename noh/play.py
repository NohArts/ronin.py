from ronin.abstract.shortcuts import *

#*************************************************************************

@click.group()
@click.option('--path', default=None)
@click.pass_context
def cli(ctx, *args, **kwargs):
    ctx.obj.update({
        'rpath': kwargs['path'],
        'neuro': NeuroChip(**kwargs),
    })

    ctx.obj['neuro'].preincl()
    #ctx.obj['neuro'].prepack()

    ctx.obj['neuro'].ports.declare({
        'cross': 7000,
        'socks': 7001,
        'flash': 7002,
        'topic': 7003,
    })

    for key in os.listdir(spath('worker.d')):
        if key.endswith('.json'):
            try:
                cfg = load_json(spath('worker.d',key))
            except:
                cfg = []

            for row in cfg:
                ctx.obj['neuro'].worker('tool', key, **row)

########################################################################################

@cli.command('init')
@click.pass_context
def initialize(ctx, *args, **kwargs):
    ctx.obj['neuro'].log('INFO', 0, "Started provisioning the reactor ...")

    for aspect in ctx.obj['neuro'].sequences():
        ctx.obj['neuro'].log('INFO', 1, "-> Provisioning : %s", aspect.narrow)

        aspect()

    ctx.obj['neuro'].log('INFO', 0, "Finishing provisioning the reactor ...")

    write_text('''hub: crossbar start --cbdir .
    svc: supervisord -c daemon.conf -n
    ''', 'Mainfile')

    #self.trigger('prelink')

    ctx.obj['neuro'].log('INFO', 0, "Reactor provisionned at : %s", bpath)

#***************************************************************************************

@cli.command('walk')
@click.pass_context
def endpoints(ctx, *args, **kwargs):
    links = []

    def walk_endpoints(ancestor,targets):
        for path,route in targets.iteritems():
            prev = "\t" * (len(ancestor)+1)
            item = {
                'type': route['type'],
                'link': '/'.join(['http://localhost:%(cross)d' % self['ports']] + ancestor + [path,''])
            }

            if route['type']=='path':
                print "%s*) %s :" % (prev,item['link'])

                walk_endpoints(ancestor+[path], route['paths'])
            elif route['type']=='reverseproxy':
                item['type'] = 'reverse'
                item['dest'] = 'http://%(host)s:%(port)d/' % route

                item['host'] = route['host']
                item['port'] = route['port']
                item['path'] = route['path']

                print "%s=> %s at %s" % (prev,item['link'],item['dest'])

                links.append(item)
            elif route['type']=='static':
                item['type'] = 'assets'
                item['path'] = route['directory']

                print "%s-> %s" % (prev,item['path'])

                links.append(item)

    #walk_endpoints([], self['paths'])

    write_json(links, 'endpoints.json')

#***************************************************************************************

@cli.command('commit')
@click.option('--add','-a', default=False)
@click.option('--message','-m', default="...")
@click.pass_context
def git_commit(ctx, *args, **kwargs):
    if ctx.obj['repo'].is_dirty():
        if kwargs['add']:
            shell('git','add','--all')

        shell('git','commit','-a','-m',kwargs['message'])
    else:
        print "clean"

##########################################################################

@cli.command('electron')
#@click.option('--add','-a', default=False)
#@click.option('--message','-m', default="...")
@click.pass_context
def render_electron(ctx, *args, **kwargs):
    target = [
        '%s/%s.html' % (prefix,entry)
        for prefix,target in [
            ('console', ['index',
                'backbone','parse','solid',
                'cypher','graphql','sparql','sql',
            ]),
            ('explore', ['index','gallery','media','plugin','typos']),
            ('linked',  ['index','corpora','dialect','domains','grammar']),
            ('person',  ['index','agenda','history','inbox','theques','vcard']),
            ('records', ['index','listing','metrics','tabular']),
        ]
        for entry in target
    ]

    for path in target:
        render_file('', path, rpath('', path))

#***************************************************************************************

@cli.command('caches')
@click.option('--add','-a', default=False)
@click.option('--message','-m', default="...")
@click.pass_context
def download_files(ctx, *args, **kwargs):
    if ctx.obj['repo'].is_dirty():
        if kwargs['add']:
            shell('git','add','--all')

        shell('git','commit','-a','-m',kwargs['message'])
    else:
        print "clean"

##########################################################################

if __name__ == '__main__':
    cli(
        obj={
            'debug': True,
            #'key': 'value',
            'python': '/usr/bin/python',
            'pip':    '/usr/bin/pip',
        }
    )

