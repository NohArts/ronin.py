from ronin.abstract.working import *

##########################################################################

@Factory.register('doc','firebase')
class FireBase(Instance):
    ASSEMBLY = 'archives.pkg'

    def prepare(self, *args, **kwargs):
        self._work = {
        }

    def exports(self):
        resp = "\n".join(self.listing.keys())

        return resp

#*************************************************************************

@Factory.register('doc','parse')
class ParseServer(Instance):
    ASSEMBLY = 'golang.pkg'

    def prepare(self, *args, **kwargs):
        self._work = {
        }

    def exports(self):
        return resp

