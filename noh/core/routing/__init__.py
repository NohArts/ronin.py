from ronin.abstract.linking import *

##########################################################################

@Factory.register('web','static')
class WebFolder(WebRoute):
    def prepare(self, *args, **kwargs):
        self._work = {
        }

    @property
    def __cross__(self):
        return {
        }

    @property
    def __redbird__(self):
        return {
        }

#*************************************************************************

@Factory.register('web','proxy')
class WebProxy(WebRoute):
    def prepare(self, *args, **kwargs):
        self._work = {
        }

    @property
    def __cross__(self):
        return {
        }

    @property
    def __redbird__(self):
        return {
        }

#*************************************************************************

@Factory.register('web','fcgi')
class Web_FastCGI(WebRoute):
    def prepare(self, *args, **kwargs):
        self._work = {
        }

    @property
    def __cross__(self):
        return {
        }

    @property
    def __redbird__(self):
        return {
        }

#*************************************************************************

@Factory.register('web','wsgi')
class Web_WSGI(WebRoute):
    def prepare(self, *args, **kwargs):
        self._work = {
        }

    @property
    def __cross__(self):
        return {
        }

    @property
    def __redbird__(self):
        return {
        }

#*************************************************************************

@Factory.register('web','php7')
class Web_PHP7(WebRoute):
    def prepare(self, *args, **kwargs):
        self._work = {
        }

    @property
    def __cross__(self):
        return {
        }

    @property
    def __redbird__(self):
        return {
        }

#*************************************************************************

@Factory.register('web','custom')
class Web_Custom(WebRoute):
    def prepare(self, *args, **kwargs):
        self._work = {
        }

    @property
    def __cross__(self):
        return {
        }

    @property
    def __redbird__(self):
        return {
        }

