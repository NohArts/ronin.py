from ronin.abstract.shortcuts import *

#******************************************************************************

#import PyExecJS

##########################################################################

@Reactor.exe.supports('python')
class PyEval(Reactor.exe.Engine):
    def prepare(self, *args, **kwargs):
        pass

    #*********************************************************************

    def execute(self, source, **context):
        return self.emulate(source, self.context,context)

    #*********************************************************************

    def emulate(self, source, multiverse, universe):
        exec source in multiverse, universe

        return None,globals(),locals()

