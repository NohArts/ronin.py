from reactor.shortcuts import *

################################################################################

@Reactor.social.register_api('facebook')
class Facebook(Reactor.social.API):
    def prepare(self):
        self.register('user', FacebookUser)
        self.register('page', FacebookPage)

    def request(self, method, url, params={}, payload=None, headers={}):
        url = 'https://graph.facebook.com' + url

        params['access_token'] = self.creds['access_token']

        return self.http_req(method, url, params, payload, headers)

    @property
    def profile(self):
        resp = api.http_get('/me', params={
            'fields':       'id,name,location,picture',
        })

        return req

#*******************************************************************************

@Reactor.social.register_resource('facebook','user')
class FacebookUser(Reactor.social.Resource):
    @classmethod
    def listing(cls, api, **query):
        #Reactor.wamp.publish(u"reactor.perso.social.knows", 'facebook', entry)

        resp = api.http_get('/me/accounts', params={ })

        for entry in resp['data']:
            yield entry

        resp = api.http_get('/me/likes', params={ })

        for entry in resp['data']:
            yield entry

    @classmethod
    def narrow(cls, api, **uid):
        pass

    NARROW = ['id']

    def prepare(self):
        pass

#*******************************************************************************

@Reactor.social.register_resource('facebook','page')
class FacebookPage(Reactor.social.Resource):
    @classmethod
    def listing(cls, api, **query):
        #Reactor.wamp.publish(u"reactor.perso.social.knows", 'facebook', entry)

        resp = api.http_get('/me/friends', params={ 'fields': 'id,name,location,picture' })

        for entry in resp['data']:
            yield entry

        resp = api.http_get('/me/follows', params={ 'fields': 'id,name,location,picture' })

        for entry in resp['data']:
            yield entry

    @classmethod
    def narrow(cls, api, **uid):
        pass

    NARROW = ['id']

    def prepare(self):
        pass

################################################################################

@Reactor.social.register_api('instagram')
class Instagram(Reactor.social.API):
    def prepare(self):
        self.register('user', InstagramUser)

    def request(self, method, url, params={}, data=None, headers={}):
        url = 'https://api.instagram.com/%s/%s' % (self.VERSION, url)

        params['access_token'] = self.creds['access_token']

        return self.http_req(method, url, params, data, headers)

    VERSION = 'v1'

    @property
    def profile(self):
        resp = self.http_get('users/%s/' % 'self')

        return resp

#*******************************************************************************

@Reactor.social.register_resource('instagram','user')
class InstagramUser(Reactor.social.Resource):
    @classmethod
    def listing(cls, api, **query):
        resp = api.http_get('users/%s/follows' % 'self')

        for person in resp['data']:
            #Reactor.wamp.publish(u"reactor.perso.social.knows", 'instagram', profile, person, RATINGs[depth+1])

            yield person

    @classmethod
    def narrow(cls, api, **uid):
        pass

    NARROW = ['id']

    def prepare(self):
        pass

