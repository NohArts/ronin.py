from . import Corporate
from . import Multimedia
from . import Productivity
from . import Misc

DEPRECATED = (
    'social_core.backends.angel.AngelOAuth2',
    'social_core.backends.appsfuel.AppsfuelOAuth2',
    'social_core.backends.beats.BeatsOAuth2',
    'social_core.backends.behance.BehanceOAuth2',
    'social_core.backends.box.BoxOAuth2',
    'social_core.backends.clef.ClefOAuth2',
    'social_core.backends.dailymotion.DailymotionOAuth2',
    'social_core.backends.deezer.DeezerOAuth2',
    'social_core.backends.disqus.DisqusOAuth2',
    'social_core.backends.douban.DoubanOAuth2',
    'social_core.backends.eveonline.EVEOnlineOAuth2',
    'social_core.backends.fitbit.FitbitOAuth2',
    'social_core.backends.jawbone.JawboneOAuth2',
    'social_core.backends.kakao.KakaoOAuth2',
    'social_core.backends.live.LiveOAuth2',
    'social_core.backends.mendeley.MendeleyOAuth2',
    'social_core.backends.mineid.MineIDOAuth2',
    'social_core.backends.nationbuilder.NationBuilderOAuth2',
    'social_core.backends.odnoklassniki.OdnoklassnikiOAuth2',
    'social_core.backends.podio.PodioOAuth2',
    'social_core.backends.rdio.RdioOAuth2',
    'social_core.backends.readability.ReadabilityOAuth',
    'social_core.backends.runkeeper.RunKeeperOAuth2',
    'social_core.backends.sketchfab.SketchfabOAuth2',
    'social_core.backends.skyrock.SkyrockOAuth',
    'social_core.backends.stocktwits.StocktwitsOAuth2',
    'social_core.backends.stripe.StripeOAuth2',
    'social_core.backends.thisismyjam.ThisIsMyJamOAuth1',
    'social_core.backends.tripit.TripItOAuth',
    'social_core.backends.yandex.YandexOAuth2',
    'social_core.backends.moves.MovesOAuth2',
    'social_core.backends.vend.VendOAuth2',
)

