from reactor.helpers import *

################################################################################

@Reactor.social.register_auth('meetup', 'MeetupOAuth2',
    register_url='https://secure.meetup.com/meetup_api/oauth_consumers/create',
    overview_url='',
mode='oauth2')
def detect_meetup(cfg):
    yield 'KEY', cfg.get('key', 'aab863dea38f889f4085048667f62b50')
    yield 'SECRET', cfg.get('pki', '507a4b7edcb6fb3194f4d01c24f207e0')

################################################################################

@Reactor.social.register_auth('podio', 'PodioOAuth2',
    register_url='',
    overview_url='',
mode='oauth2')
def detect_mailru(cfg):
    yield 'KEY', cfg.get('key', 'aab863dea38f889f4085048667f62b50')
    yield 'SECRET', cfg.get('pki', '507a4b7edcb6fb3194f4d01c24f207e0')

#*******************************************************************************

@Reactor.social.register_auth('yammer', 'YammerOAuth2',
    register_url='',
    overview_url='',
mode='oauth2')
def detect_yammer(cfg):
    yield 'KEY', cfg.get('key', 'c65e3f78706e63f4efe83a12a9329ad5')
    yield 'SECRET', cfg.get('pki', '9623e3ab782c0a615ecbf838ccf369cfafc7f5f326ca40df764277082dfc1c01')

