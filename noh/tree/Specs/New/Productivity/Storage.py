from reactor.helpers import *

################################################################################

@Reactor.social.register_auth('dropbox', 'DropboxOAuth2',
    register_url='',
    overview_url='',
mode='oauth2')
def detect_dropbox(cfg):
    yield 'OAUTH2_KEY', cfg.get('key', 'f01f0r4p12xev8x')
    yield 'OAUTH2_SECRET', cfg.get('pki', 'vr4gjv405sy8mp0')

