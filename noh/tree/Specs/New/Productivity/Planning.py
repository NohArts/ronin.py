from reactor.helpers import *

################################################################################

from reactor.helpers import *

################################################################################

@Reactor.social.register_auth('asana', 'AsanaOAuth2',
    register_url='',
    overview_url='',
mode='oauth2')
def detect_asana(cfg):
    yield 'KEY', cfg.get('key', 'c65e3f78706e63f4efe83a12a9329ad5')
    yield 'SECRET', cfg.get('pki', '9623e3ab782c0a615ecbf838ccf369cfafc7f5f326ca40df764277082dfc1c01')

#*******************************************************************************

@Reactor.social.register_auth('trello', 'TrelloOAuth',
    register_url='https://trello.com/1/appKey/generate',
    overview_url='',
mode='oauth')
def detect_trello(cfg):
    yield 'KEY', cfg.get('key', 'c65e3f78706e63f4efe83a12a9329ad5')
    yield 'SECRET', cfg.get('pki', '9623e3ab782c0a615ecbf838ccf369cfafc7f5f326ca40df764277082dfc1c01')

    yield 'APP_NAME', 'Uchikoma Connect'
    yield 'EXPIRATION', '30days'

#*******************************************************************************

@Reactor.social.register_auth('wunderlist', 'WunderlistOAuth2',
    register_url='',
    overview_url='',
mode='oauth2')
def detect_wunderlist(cfg):
    yield 'KEY', cfg.get('key', 'c65e3f78706e63f4efe83a12a9329ad5')
    yield 'SECRET', cfg.get('pki', '9623e3ab782c0a615ecbf838ccf369cfafc7f5f326ca40df764277082dfc1c01')

