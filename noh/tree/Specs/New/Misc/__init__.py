from . import Web
from . import Pay

from . import Dev
from . import Ops

from . import Geo
from . import IoT

from . import Edu
from . import Bio

