from reactor.helpers import *

################################################################################

@Reactor.social.register_auth('coursera', 'CourseraOAuth2',
    register_url='',
    overview_url='',
mode='oauth2')
def detect_coursera(cfg):
    yield 'KEY', cfg.get('key', 'd2c35ed24d9b8ae3aa0f')
    yield 'SECRET', cfg.get('pki', 'd2c35ed24d9b8ae3aa0f')

#*******************************************************************************

@Reactor.social.register_auth('khanacademy', 'KhanAcademyOAuth1',
    register_url='',
    overview_url='',
mode='oauth2')
def detect_khanacademy(cfg):
    yield 'KEY', cfg.get('key', 'd2c35ed24d9b8ae3aa0f')
    yield 'SECRET', cfg.get('pki', 'd2c35ed24d9b8ae3aa0f')

