from reactor.helpers import *

################################################################################

@Reactor.social.register_auth('lastfm', 'LastFmAuth',
    register_url='http://www.last.fm/api/account/create',
    overview_url='',
mode='oauth')
def detect_lastfm(cfg):
    yield 'KEY', cfg.get('key', 'b4e861a03720e9eb6fe16ea8af7b5e41')
    yield 'SECRET', cfg.get('pki', 'e7b16a4dec7bfcab0d2ed19ecb74bdf3')

################################################################################

@Reactor.social.register_auth('soundcloud', 'SoundcloudOAuth2',
    register_url='',
    overview_url='',
mode='oauth2')
def detect_soundcloud(cfg):
    yield 'KEY', cfg.get('key', 'aab863dea38f889f4085048667f62b50')
    yield 'SECRET', cfg.get('pki', '507a4b7edcb6fb3194f4d01c24f207e0')

#*******************************************************************************

@Reactor.social.register_auth('mixcloud', 'MixcloudOAuth2',
    register_url='http://www.mixcloud.com/developers',
    overview_url='',
mode='oauth2')
def detect_mixcloud(cfg):
    yield 'KEY', cfg.get('key', 'ze6RFZKqUWdMnnH5Mu')
    yield 'SECRET', cfg.get('pki', 'J4d8PTUWF4JfpBmH656a7G6QLgXc5n34')

    yield 'EXTRA_DATA', [
        ('username', 'username'),
        ('name', 'name'),
        ('pictures', 'pictures'),
        ('url', 'url')
    ]

#*******************************************************************************

#@Reactor.social.register_auth('spotify', 'SpotifyOAuth2',
#    register_url='',
#    overview_url='',
#mode='oauth2')
#def detect_spotify(cfg):
#    yield 'KEY', cfg.get('key', 'xxxxxx')
#    yield 'SECRET', cfg.get('pki', 'xxxxxx')

