from . import HTTP
from . import SQL

from . import S3
from . import WebDAV

from . import BoxNet
from . import DropBox

def has_keys(target, *fields):
    return reduce(operator.and_, [
        (key in target) for key in fields
    ] or [False])

from fs.mountfs import MountFS

class Manager(object):
    def __init__(self, config_path):
        self._cfg = yaml.load(open(config_path))

        self._vfs = None

        self._src = {}
        self._mnt = {}
        self._rpc = {}

        for obj in cfg.get('source', []):
            if has_keys(obj, 'name', 'link'):
                self._src[obj['name']] = obj

    config = property(lambda self: self._cfg)

    def get_fs(self, name, link, **opts):
        uri = urlparse(link)

        if uri.scheme=='debug':
            return 
        elif uri.scheme in ('temp','tempfs'):
            return 
        elif uri.scheme in ('ram','mem','memory'):
            return 
        elif uri.scheme in ('file','osfs'):
            return 
        elif uri.scheme in ('ftp','sftp'):
            return 
        elif uri.scheme in ('http','https'):
            return 
        elif uri.scheme in ('dav','webdav'):
            return 
        elif uri.scheme in ('s3','s3fs','minio'):
            return 
        elif uri.scheme in ('boxnet','dropbox'):
            return 
        elif uri.scheme in ('cifs','smbfs','smb','samba'):
            return 
        else:
            print "Unsupported FS : %s" % link

        return None

    source = property(lambda self: self._src)
    mounts = property(lambda self: self._mnt)

    def compose(self, mapping):
        resp = None

        if type(mapping) in (tuple,set,frozenset,list):
            resp = MultiFS()

            for target in mapping:
                resp.addfs(target)

        elif hasattr(mapping, 'iteritems'):
            resp = MountFS()

            for prefix,target in mapping.iteritems():
                resp.mountdir(prefix, target)

        return resp

    @property
    def rootfs(self):
        if self._vfs is None:
            self._vfs = self.compose({
                
            })

        return self._vfs

    def __call__(self, port=8080, addr=""):
        srv = RPCFSServer(self.rootfs,(addr,port))

        srv.serve_forever()

if __name__=='__main__':
    target = Manager('filesystem.yaml')

    target(8080)

