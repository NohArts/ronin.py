from ronin.kata.practice import *

################################################################################

@Reactor.wsgi.register('/')
class Landing(Reactor.wsgi.Resource):
    def get(self):
        return render_page(None, 'landing', None, title="Welcome to the IT-Tohoku")

#*************************************************************************

@Reactor.wsgi.register('/about')
class AboutUs(Reactor.wsgi.Resource):
    def get(self):
        return render_page(None, 'about-us', None, title="About us")

#*************************************************************************

@Reactor.wsgi.register('/contact')
class Contact(Reactor.wsgi.Resource):
    def get(self):
        return render_page(None, 'contact', None, title="Contact or Visit")

##########################################################################

@Reactor.wsgi.register('/health/')
class Health_Homepage(Reactor.wsgi.Resource):
    def get(self):
        return render_page('health', 'homepage', None, title="Platform's Status")

#*************************************************************************

@Reactor.wsgi.register('/health/digests')
class Health_Digests(Reactor.wsgi.Resource):
    def get(self):
        return render_page('health', 'digests', None, title="Digestive Report")

#*************************************************************************

@Reactor.wsgi.register('/health/insight')
class Health_Insight(Reactor.wsgi.Resource):
    def get(self):
        return render_page('health', 'insight', None, title="Tohoku's Insights")

#*************************************************************************

@Reactor.wsgi.register('/health/weather')
class Health_Weather(Reactor.wsgi.Resource):
    def get(self):
        return render_page('health', 'weather', None, title="Weather Map")

##########################################################################
##########################################################################

#@Reactor.wsgi.register('/<path:path>')
def static_proxy(path):
    # send_static_file will guess the correct MIME type
    return Reactor.wsgi.app.send_static_file(path)

