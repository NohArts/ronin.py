from ronin.kata.practice import *

################################################################################

@Reactor.wsgi.register('/linked/')
class Linked_Home(Reactor.wsgi.Resource):
    def get(self):
        return render_page('linked', 'homepage', None, title="")

##########################################################################
                       
@Reactor.wsgi.register('/linked/catalog')
class Linked_Catalog(Reactor.wsgi.Resource):
    def get(self):
        return render_page('linked', 'catalog', None, title="")

##########################################################################

@Reactor.wsgi.register('/linked/networks')
class Linked_Networks(Reactor.wsgi.Resource):
    def get(self):
        return render_page('linked', 'networks', None, title="")

##########################################################################

@Reactor.wsgi.register('/linked/records')
class Linked_Records(Reactor.wsgi.Resource):
    def get(self):
        return render_page('linked', 'records', None, title="")

