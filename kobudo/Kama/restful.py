from ronin.kata.practice import *

################################################################################

@Reactor.wsgi.register('/coding/backends.json')
@login_required
def ajax_backend():
    resp = []

    return jsonify(resp)

@Reactor.wsgi.register('/coding/clusters.json')
@login_required
def ajax_cluster():
    resp = []

    return jsonify(resp)

@Reactor.wsgi.register('/coding/programs.json')
@login_required
def ajax_program():
    resp = []

    return jsonify(resp)

@Reactor.wsgi.register('/coding/toolkits.json')
@login_required
def ajax_toolkit():
    resp = []

    return jsonify(resp)

################################################################################

@Reactor.wsgi.register('/opsys/images.json')
@login_required
def ajax_images():
    resp = []

    return jsonify(resp)

@Reactor.wsgi.register('/opsys/hybrid.json')
@login_required
def ajax_hybrid():
    resp = []

    return jsonify(resp)

@Reactor.wsgi.register('/opsys/routing.json')
@login_required
def ajax_routing():
    resp = []

    return jsonify(resp)

@Reactor.wsgi.register('/opsys/studio.json')
@login_required
def ajax_studio():
    resp = []

    return jsonify(resp)

################################################################################

