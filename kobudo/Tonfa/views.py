from ronin.kata.practice import *

from .concept import *

##########################################################################

#@Reactor.wsgi.register('/compoze/')
@Reactor.wsgi.register('/engine/')
class Engine_Home(Reactor.wsgi.Resource):
    def get(self):
        return render_page('engine', 'homepage', dict(
            listing= Runtime.objects.all(),
        ), title="")

##########################################################################

@Reactor.wsgi.register('/engine/~<language>/')
class Engine_Lang_Home(Reactor.wsgi.Resource):
    def get(self, language):
        wrp = Runtime.objects.get(alias=language)

        return render_page('engine', 'overview', dict(
            wrapper=wrp,
            listing= Runtime.objects.get(engine=wrp),
        ), title="")

#*************************************************************************

@Reactor.wsgi.register('/engine/~<language>/packager')
class Engine_Lang_Packager(Reactor.wsgi.Resource):
    def get(self, language):
        return render_page('engine', 'packager', None, title="")

