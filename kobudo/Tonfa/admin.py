from ronin.kata.practice import *

################################################################################

@Reactor.wsgi.register('/assets/',name='Assets',category='Explorer')
class Assets(Reactor.wsgi.Explorer):
    PATH = RPATH('workdir','cdn','assets')

#******************************************************************************

@Reactor.wsgi.register('/media/',name='Multimedia',category='Explorer')
class Multimedia(Reactor.wsgi.Explorer):
    PATH = RPATH('workdir','cdn','media')

################################################################################

#@Reactor.wsgi.register('GraphQL',category='Console')
class GraphQL(Reactor.wsgi.Manager):
    @Reactor.wsgi.expose('/')
    def view(self):
        return self.render('tools/graphql.html')

    @Reactor.wsgi.expose('/query')
    def query(self):
        return self.render('test.html')

#******************************************************************************

#@Reactor.wsgi.register('SparQL',category='Console')
class YasGUI(Reactor.wsgi.Manager):
    @Reactor.wsgi.expose('/')
    def view(self):
        return self.render('tools/sparql.html')

    @Reactor.wsgi.expose('/query')
    def query(self):
        return self.render('test.html')

