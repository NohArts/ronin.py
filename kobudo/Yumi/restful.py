from ronin.kata.practice import *

from .helpers import *

################################################################################

@Reactor.wsgi.register('/social/providers.json')
@login_required
def ajax_providers():
    resp = []

    for smp in SocialMeta.from_alls():
        if len(smp.environ):
            resp.append({
                'owner':    smp.owner,
                'cloud':    smp.alias,

                'api_key':  smp.environ.get('key',''),
                'secret':   smp.environ.get('pki',''),
                'scopes':   smp.environ.get('acl',''),

                'callback': smp.callback,
            })

    return jsonify(resp)

################################################################################

@Reactor.wsgi.register('/social/tokens.json')
@login_required
def ajax_tokens():
    resp = []

    for smp in SocialMeta.from_user():
        raw = item.extra_data

        obj = {
            'owner':    smp.owner,
            'cloud':    smp.alias,
            'user':     smp.assoc.uid,

            'when':     None,
            'expire':   None,

            'token':    None,
            'scopes':   [],
            'salt':     None,
        }

        for nrw,lst in [
            ('token',  ['access_token']),
            ('expire', ['expires','expire_at','expires_at']),

            ('when',   []),
            ('salt',   []),
        ]:
            for key in lst:
                if key in raw:
                    obj[nrw] = raw[key]
                elif 'access_token' in raw:
                    if key in raw['access_token']:
                        obj[nrw] = raw[key]

        resp.append(obj)

    return jsonify(resp)

################################################################################

@Reactor.wsgi.register('/social/particles.json')
def ajax_particles():
    dest = SocialMeta.from_alls()

    if g.user.is_authenticated:
        dest = SocialMeta.from_user()

    resp = []

    for smp in dest:
        for key,sch in smp.entities.iteritems():
            entry = {
                'provider': smp.alias,
                'slugname': key,
                #'ancestor': None, #key,

                'columns':  [],
            }

            for col in sch.get('columns',[]):
                if 'type' not in col:
                    col['type'] = 'string'

                entry['columns'].append(col)

            #operate={
            #    'list': dict(link=""),
            #    'post': dict(link=""),
            #    'read': dict(link=""),
            #    'remv': dict(link=""),
            #},
            #routing={
            #    'desktop': {},
            #    'android': {},
            #    'iphones': {},
            #},

            resp.append(entry)

    return jsonify(resp)

