from ronin.kata.practice import *

################################################################################

@Reactor.data.register(None, category="xxxxxx")
class Backend(Reactor.data.Document):
    owner    = ReferenceField(Identity)
    account  = EmailField()
    enabled  = BooleanField(default=True)

    provider = StringField(max_length=200)
    username = StringField(max_length=200)
    password = StringField(max_length=200, default='')

from social_flask_mongoengine import models

