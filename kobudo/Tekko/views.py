from .helpers import *

##########################################################################

@Reactor.wsgi.register('/speak/')
class Speak_Home(Reactor.wsgi.Resource):
    def get(self):
        lst = [
            './word_tokenize',
            './sent_tokenize',
            './pos_tag',
            './stem/<method>',
            './lemmatize/<method>',
            './stanfordNER',
        ]

        return render_page('speak', 'homepage', None, title="")

##########################################################################

@Reactor.wsgi.register('/think/')
class Think_Home(Reactor.wsgi.Resource):
    def get(self):
        return render_page('think', 'homepage', None, title="")

