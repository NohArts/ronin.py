from ronin.kata.practice import *

from .helpers import *

##########################################################################

@Reactor.wsgi.register('/speak/tokenize',methods=['POST'])
def word_tokenize():
    return wrapper.word_tokenize(request.data)

@Reactor.wsgi.register('/speak/sentence',methods=['POST'])
def sent_tokenize():
    return wrapper.sent_tokenize(request.data)

@Reactor.wsgi.register('/speak/pos_tag',methods=['POST'])
def pos_tag():
    return wrapper.pos_tag(request.data)

@Reactor.wsgi.register('/speak/stemmer/<method>',methods=['POST'])
def stem(method):
	return stemmer(method,request.data)

@Reactor.wsgi.register('/speak/lemmata/<method>',methods=['POST'])
def lem(method):
	return lemmatize(method,request.data) 

@Reactor.wsgi.register('/speak/nertag',methods=['POST'])
def nertagger():
	return tagger(request.data)    

##########################################################################

@Reactor.wsgi.register('/speak/corporas.json')
@login_required
def ajax_corporas():
    resp = []

    return jsonify(resp)

@Reactor.wsgi.register('/speak/dialects.json')
@login_required
def ajax_dialects():
    resp = []

    return jsonify(resp)

@Reactor.wsgi.register('/speak/grammars.json')
@login_required
def ajax_grammars():
    resp = []

    return jsonify(resp)

@Reactor.wsgi.register('/speak/glossary.json')
@login_required
def ajax_glossary():
    resp = []

    return jsonify(resp)

@Reactor.wsgi.register('/speak/vocublar.json')
@login_required
def ajax_vocublar():
    resp = []

    return jsonify(resp)

