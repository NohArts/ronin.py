from ronin.kata.practice import *

################################################################################

@Reactor.wsgi.register('/people/')
class People_Home(Reactor.wsgi.Resource):
    def get(self):
        return render_page('people', 'homepage', None, title="")

#*************************************************************************

@Reactor.wsgi.register('/people/<narrow>/?')
class People_View(Reactor.wsgi.Resource):
    def get(self):
        return render_page('people', 'overview', None, title="")

##########################################################################

@Reactor.wsgi.register('/cables/')
class Cables_Home(Reactor.wsgi.Resource):
    def get(self):
        return render_page('cables', 'homepage', None, title="")

#*************************************************************************

@Reactor.wsgi.register('/agenda/')
class Agenda_Home(Reactor.wsgi.Resource):
    def get(self):
        return render_page('agenda', 'homepage', None, title="")

#*************************************************************************

@Reactor.wsgi.register('/social/')
class Social_Home(Reactor.wsgi.Resource):
    def get(self):
        return render_page('social', 'homepage', None, title="")

##########################################################################

@Reactor.wsgi.register('/history/')
class History_Home(Reactor.wsgi.Resource):
    def get(self):
        return render_page('history', 'homepage', None, title="")

#*************************************************************************

@Reactor.wsgi.register('/history/converse')
class History_Converse(Reactor.wsgi.Resource):
    def get(self):
        return render_page('history', 'converse', None, title="")

#*************************************************************************

@Reactor.wsgi.register('/history/navigate')
class History_Navigate(Reactor.wsgi.Resource):
    def get(self):
        return render_page('history', 'navigate', None, title="")

#*************************************************************************

@Reactor.wsgi.register('/history/telecoms')
class History_Telecom(Reactor.wsgi.Resource):
    def get(self):
        return render_page('history', 'telecoms', None, title="")

