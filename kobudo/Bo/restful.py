from ronin.kata.practice import *

################################################################################

@Reactor.wsgi.register('/history/bookmark.json')
@login_required
def ajax_bookmark():
    qrs,lst = {},[]

    from ronin.kobudo.Yumi.helpers import SocialMeta

    idp = SocialAPI.from_name('pocket')

    if idp is not None:
        qrs = {}

        req = requests.get('https://getpocket.com/v3/get', data={
            'consumer_key': idp.psa_env('KEY'),
            'access_token': idp.psa_env('KEY'),
        })

        raw = req.json()

        lst = raw['list']

    return jsonify(lst)

##########################################################################

@Reactor.wsgi.register('/history/navigate.json')
@login_required
def ajax_navigate():
    resp = []

    for item in g.user.social_auth.all():
        raw,cfg = item.extra_data,{
            'owner':    g.user.username,
            'cloud':    item.provider,
            'user':     item.uid,

            'when':     None,
            'expire':    None,

            'token':    None,
            'scopes':   [],
            'salt':    None,
        }

        if 'access_token' in raw:
            raw = raw['access_token']

        resp.append(cfg)

    return jsonify(resp)

