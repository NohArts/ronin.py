cffi
crossbar
psycopg2
click
celery[redis]

-r bushido/base/requirements.txt
-r bushido/clue/requirements.txt
-r bushido/data/requirements.txt
#-r bushido/disk/requirements.txt
-r bushido/exec/requirements.txt
-r bushido/flow/requirements.txt
-r bushido/hook/requirements.txt
-r bushido/idea/requirements.txt
-r bushido/meta/requirements.txt
-r bushido/mind/requirements.txt
-r bushido/play/requirements.txt
-r bushido/pool/requirements.txt
-r bushido/tree/requirements.txt

-r kobudo/requirements.txt
-r kyodo/requirements.txt
-r kata/requirements.txt
-r noh/requirements.txt

davclient
PyWebDAV
WsgiDAV
