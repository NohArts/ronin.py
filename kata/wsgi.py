#!/usr/bin/env python

from ronin.kata.practice import *

#*************************************************************************

import ronin.kobudo

#*************************************************************************

Reactor.wsgi.lunch()

##########################################################################

app = Reactor.wsgi.app
application = Reactor.wsgi.app

#*************************************************************************

if __name__ == '__main__':
    from flask_script import Server, Manager, Shell

    mgr = Manager(Reactor.wsgi.app)

    mgr.add_command('runserver', Server())
    mgr.add_command('shell', Shell(make_context=lambda: {
        'app': Reactor.wsgi.app,

        'orm':  Reactor.data.sql,
        'odm':  Reactor.data.nosql,
    }))

    mgr.run()

