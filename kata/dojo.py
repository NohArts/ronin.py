#!/usr/bin/env python

from ronin.kata.wsgi import *

#from ronin.provision.shortcuts import *

########################################################################################

@click.group()
@click.option('--path', default=None)
@click.pass_context
def cli(ctx, *args, **kwargs):
    ctx.obj.update({
        'rpath': kwargs['path'],
    })

    Reactor.hub.loader('dojo', **locals())

#***************************************************************************************

@cli.command('manifest')
@click.pass_context
def hub_meta(ctx, *args, **kwargs):
    Reactor.hub.persist()

    Reactor.pkg.manifest(None)

    write_text("""web: python -m ronin.kata.dojo bootstrap
worker: python -m ronin.kata.dojo servant
release: python -m ronin.kata.dojo commit""", 'Procfile')

#***************************************************************************************

@cli.command('deploy')
@click.pass_context
def hub_post(ctx, *args, **kwargs):
    Reactor.pkg.download()

    Reactor.pkg.manifest(None)
    for entry in Reactor.hub.setup:
        if type(entry) in (str,unicode):
            os.system(entry)
        elif callable(entry):
            entry()

#***************************************************************************************

@cli.command('commit')
@click.pass_context
def hub_hook(ctx, *args, **kwargs):
    pass

########################################################################################

@cli.command('bootstrap')
@click.pass_context
def hub_start(ctx, *args, **kwargs):
    Reactor.hub.command('start')

#***************************************************************************************

@cli.command('teardown')
@click.pass_context
def hub_stop(ctx, *args, **kwargs):
    Reactor.hub.command('stop')

#***************************************************************************************

@cli.command('checkup')
@click.pass_context
def hub_status(ctx, *args, **kwargs):
    Reactor.hub.command('status')

########################################################################################

@cli.command('servant')
@click.pass_context
def errant_knight(ctx, *args, **kwargs):
    os.system('rq worker')

#***************************************************************************************

@cli.command('exposefs')
#@click.option('--shout/--no-shout', default=False)
@click.option('-t', '--type', type=click.Choice(['rpc', 'http', 'sftp']))
@click.option('-a', '--addr', default="0.0.0.0")
@click.option('-p', '--port', type=int, default=8021)
#@click.option('-v', '--verbose', count=True)
@click.pass_context
def expose_fs(ctx, *packages, **kwargs):
    ctx.obj.update(kwargs)

    ctx.obj['mode'] = ctx.obj['type'].upper()

    click.echo("Serving VFS using '%(mode)s' on port : %(port)d" % ctx.obj)

    Reactor.disk.expose('rpc', port, addr)

########################################################################################

if __name__ == '__main__':
    cli(
        obj={
            'hub': None,
        },
    )

