from ronin.abstract.shortcuts import *

##########################################################################

from flask import Flask, g, url_for, render_template, redirect, request, jsonify, make_response, Response

from flask import url_for as common_url_for

#******************************************************************************

from flask_login import login_required, logout_user, LoginManager, current_user

from flask_restful import fields, marshal_with

#******************************************************************************

from mongoengine import StringField, EmailField, BooleanField, URLField, ListField, DictField, ReferenceField

#******************************************************************************

from social_flask.utils import load_strategy
from social_flask.routes import social_auth
from social_flask.template_filters import backends
from social_flask_mongoengine.models import init_social

################################################################################

from flask_login import UserMixin
from social_flask_mongoengine.models import FlaskStorage

#*************************************************************************

def render_page(narrow, alias, payload, **context):
    p = Reactor.wsgi.WebPage(narrow, alias, payload, **context)

    r = p.render(**context)

    return make_response(r, 200)

