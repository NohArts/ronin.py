from ronin.kata.wsgi import *

##########################################################################

@click.group() # context_settings=CONTEXT_SETTINGS)
@click.option('--debug/--no-debug', default=False)
#@click.option('-p', '--platform', default='local') # , choices=['local','heroku'])
#@click.option('-c', '--container', default='django-rq') # , choices=['django-rq','hubot','luigi','airflow'])
@click.pass_context
def cli(ctx, debug, *args, **kwargs):
    click.echo("Debug mode is %s" % ('on' if debug else 'off'))

#***************************************************************************************

@cli.command('setup')
#@click.option('command', nargs='+')
@click.pass_context
def bootstrap(ctx, *args, **kwargs):
    pass

##########################################################################

def abort_if_false(ctx, param, value):
    if not value:
        ctx.abort()

@cli.command()
@click.option('--yes', is_flag=True, callback=abort_if_false, expose_value=False, prompt='Are you sure you want to drop the db?')
@click.pass_context
def dropdb(ctx, *args, **kwargs):
    ctx.obj.update(kwargs)

    click.echo('Dropped all tables!')

    target = Manager('filesystem.yaml')

    target(8080)

########################################################################################

@cli.command('shell')
#@click.option('command', nargs='+')
@click.pass_context
def posix_bus(ctx, *args, **kwargs):
    pass

#***************************************************************************************

@cli.command('invite')
#@click.option('command', nargs='+')
@click.pass_context
def interpreter(ctx, *args, **kwargs):
    pass

#***************************************************************************************

@cli.command('execute')
@click.option('target', nargs='+')
@click.option('--type','-t', default='eval')
@click.option('--env','-e', multiple=True)
@click.option('--db','-d', multiple=True)
@click.option('--ext','-x', multiple=True)
@click.pass_context
def compilator(ctx, *targets, **kwargs):
    pass

########################################################################################

rootFS = None

#***************************************************************************************

@cli.command('expose')
#@click.option('--shout/--no-shout', default=False)
@click.option('-t', '--type', type=click.Choice(['rpc', 'http', 'sftp']))
@click.option('-a', '--addr', default="0.0.0.0")
@click.option('-p', '--port', type=int, default=8021)
#@click.option('-v', '--verbose', count=True)
@click.pass_context
def expose_fs(ctx, *packages, **kwargs):
    ctx.obj.update(kwargs)

    ctx.obj['mode'] = ctx.obj['type'].upper()

    click.echo("Serving VFS using '%(mode)s' on port : %(port)d" % ctx.obj)

    Reactor.disk.expose('rpc', port, addr)

########################################################################################

if __name__ == '__main__':
    cli(
        obj={
            'vfs': current,
        },
    )

