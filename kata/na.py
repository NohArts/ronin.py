#!/usr/bin/env python

from ronin.kata.wsgi import *

#from ronin.provision.shortcuts import *

########################################################################################

#script = open('%s/etc/provision.py' % ROOT_PATH).read()

#exec script in globals(),locals()

########################################################################################

@click.group()
@click.option('--path', default=None)
@click.pass_context
def cli(ctx, *args, **kwargs):
    ctx.obj.update({
        'rpath': kwargs['path'],
        'cross': instantiate(**kwargs),
    })

    for x in ('target','person','realms','cypher','domain','secret'):
        y = 'GHOST_%s' % x.upper()

        if y not in os.environ:
            print "Missing Shell variable : %s" % y

        ctx.obj['ghost'][x] = os.environ[y]

    ctx.obj.update({
        'rpath': kwargs['path'],
        'cross': instantiate(**kwargs),
    })

    Reactor.hub.loader('dojo', **locals())

########################################################################################

@cli.command('server')
@click.option('-p', '--port', type=int, default=6000)
@click.option('-a', '--addr', default="0.0.0.0")
@click.pass_context
def hub_server(ctx, port, *args, **kwargs):
    ctx.obj['cross'].command('status')

#***************************************************************************************

@cli.command('worker')
@click.option('-n', '--name', default="default")
@click.option('-q', '--queue', multiple=True)
@click.pass_context
def run_worker(ctx, name, queue, *args, **kwargs):
    from rq import Connection, Worker

    with Connection() as conn:
        w = Worker(queue)

        w.work()

########################################################################################

@cli.command('clocks')
@click.option('-i', '--interval', type=float, default=10.0)
@click.pass_context
def run_scheduler(ctx, *args, **kwargs):
    from apscheduler.schedulers.background import BackgroundScheduler

    scheduler = BackgroundScheduler({
        'apscheduler.jobstores.mongo': {
            'type': 'mongodb',
            'url': os.environ.get('MONGOLAB_URI', 'mongodb://localhost:27017/scheduler')
        },
        'apscheduler.jobstores.default': {
            'type': 'sqlalchemy',
            'url': os.environ.get('DATABASE_URL', 'sqlite:///scheduler.sqlite')
        },
        'apscheduler.executors.default': {
            'class': 'apscheduler.executors.pool:ThreadPoolExecutor',
            'max_workers': '20'
        },
        'apscheduler.executors.processpool': {
            'type': 'processpool',
            'max_workers': '5'
        },
        'apscheduler.job_defaults.coalesce': 'false',
        'apscheduler.job_defaults.max_instances': '3',
        'apscheduler.timezone': 'UTC',
    })

    from apschedulerweb import start as apsweb_start

    #def printer(sched):
    #    print(sched)
    #
    #scheduler.add_interval_job(printer, args=['hello'], seconds=5)

    apsweb_start(scheduler, users={'user':'pass'})

#***************************************************************************************

@cli.command('implant')
@click.argument('endpoint', default=u"ws://apis.uchikoma.faith:8000/io-sockets")
@click.option('--realm', '-r', default="reactor")
@click.option('--prefix', '-p', default="reactor.webapp.common")
@click.option('--middleware', '-mw', multiple=True)
#@click.option('--suffix', '-s', default="")
@click.pass_context
#def run_implant(ctx, endpoint, realm, prefix, middleware, suffix, *args, **kwargs):
def run_implant(ctx, endpoint, realm, prefix, middleware, **opts):
    hub = ctx.obj['cross']

    mws = hub.greffons(prefix, *middleware, **opts)

    hub.runner(endpoint, realm, *mws)

#***************************************************************************************

@cli.command('stream')
@click.argument('endpoint', default=u"ws://apis.uchikoma.faith:8000/io-sockets")
@click.option('--realm', '-r', default="reactor")
@click.option('--prefix', '-p', default="reactor.webapp.common")
@click.option('--middleware', '-mw', multiple=True)
#@click.option('--suffix', '-s', default="")
@click.pass_context
#def run_implant(ctx, endpoint, realm, prefix, middleware, suffix, *args, **kwargs):
def run_implant(ctx, endpoint, realm, prefix, middleware, **opts):
    hub = ctx.obj['cross']

    mws = hub.greffons(prefix, *middleware, **opts)

    hub.runner(endpoint, realm, *mws)

########################################################################################

if __name__ == '__main__':
    cli(
        obj={
            'debug': True,
            #'key': 'value',
            'python': '/usr/bin/python',
            'pip':    '/usr/bin/pip',
        }
    )

