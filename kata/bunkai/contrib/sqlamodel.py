from ronin.kata.bunkai.contrib import DeprecatedModelView

from ronin.kata.bunkai.model.backends.sqlalchemy import ModelAdmin


class ModelView(DeprecatedModelView, ModelAdmin):
    pass
