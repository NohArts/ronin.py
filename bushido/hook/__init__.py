from ronin.abstract.shortcuts import *

#******************************************************************************

#from datafs import DataAPI

##########################################################################

@Reactor.extend('hook')
class Manager(Reactor.Extension):
    def prepare(self):
        self._que = {}

        #self._api = DataAPI(
        #     username='My Name',
        #     contact = 'my.email@example.com')

    #********************************************************************

    jobs = property(lambda self: self._vfs)
    apis = property(lambda self: self._api)

    #####################################################################

    class Command(Atomic):
        pass

    #********************************************************************

    class Scheduler(Atomic):
        pass

    #####################################################################

    class Clock(Atomic):
        pass

    #********************************************************************

    class Pattern(Atomic):
        pass

