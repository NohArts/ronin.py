from ronin.abstract.shortcuts import *

#******************************************************************************

#from datafs import DataAPI

##########################################################################

@Reactor.extend('exec')
class Manager(Reactor.Extension):
    def prepare(self):
        self._que = {}

        #self._api = DataAPI(
        #     username='My Name',
        #     contact = 'my.email@example.com')

    #********************************************************************

    jobs = property(lambda self: self._vfs)
    apis = property(lambda self: self._api)

    #####################################################################

    class Script(Atomic):
        pass

    #********************************************************************

    class Engine(Atomic):
        pass

    #####################################################################

    class Ripper(Atomic):
        pass

    #********************************************************************

    class Runtime(Atomic):
        pass

