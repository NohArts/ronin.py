from ronin.abstract.shortcuts import *

#******************************************************************************

#from datafs import DataAPI

##########################################################################

@Reactor.extend('clue')
class Manager(Reactor.Extension):
    def prepare(self):
        self._que = {}

        #self._api = DataAPI(
        #     username='My Name',
        #     contact = 'my.email@example.com')

    #********************************************************************

    jobs = property(lambda self: self._vfs)
    apis = property(lambda self: self._api)

    #####################################################################

    class Task(Atomic):
        pass

    #********************************************************************

    class Worker(Atomic):
        pass

    #####################################################################

    class Queue(Atomic):
        pass

    #********************************************************************

    class Namespace(Atomic):
        pass

