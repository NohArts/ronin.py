from ronin.abstract.shortcuts import *

from social_core.backends.oauth import OAuthAuth

from ghost.configure import RPATH

from flask import render_template

#******************************************************************************

NAME_RE = re.compile(r'([^O])Auth')

LEGACY_NAMES = ['username', 'email']

################################################################################

@Reactor.wsgi.globaltag(None,'widget')
def render_widget(narrow,alias,icon,**options):
    options['title'] = options.get('title','') or alias.capitalize()

    options['headfull'] = False

    options.update({
        'narrow' : narrow,
        'alias'  : alias,
        'icon'   : icon,
    })

    resp = render_template('widget/%s.html' % narrow, **options)

    return '\n'.join([
		'<h2><span class="social %(icon)s"><i></i>' % options,
        '</span>%(title)s</h2><hr><div class="content">' % options,
        resp,'</div>',
    ])

#******************************************************************************

@Reactor.wsgi.globaltag(None,'portlet')
def render_portlet(narrow,alias,icon,**options):
    options['headfull'] = True

    options.update({
        'narrow' : narrow,
        'alias'  : alias,
        'icon'   : icon,
    })

    return render_template('widget/%s.html' % narrow, **options)

################################################################################

@Reactor.wsgi.filtertag(None)
def backend_name(backend):
    name = backend.__name__
    name = name.replace('OAuth2', '')
    name = name.replace('OAuth1', '')
    name = name.replace('OAuth', '')
    name = name.replace('OpenId', '')
    name = name.replace('Sandbox', '')
    name = name.replace('Auth', '')
    name = name.replace('V2', '')
    name = NAME_RE.sub(r'\1 Auth', name)
    return name

#******************************************************************************

@Reactor.wsgi.filtertag(None)
def backend_class(backend):
    return backend.name.replace('-', ' ')

#******************************************************************************

@Reactor.wsgi.filtertag(None,'backend_icon','social_icon')
def icon_name(name):
    for key in ('-oauth2','-oauth','-app'):
        if name.endswith(key):
            name = name.replace(key,'')

    name = {
        'google-openidconnect': 'google',
        'stackoverflow': 'stack-overflow',

        'live': 'windows',
        'pocket': 'get-pocket',
        'vimeo': 'vimeo-square',
    }.get(name, name)

    return {
        'email': 'envelope',
        'username': 'user',

        'docker': 'rocket',
        'coinbase': 'bitcoin',
        'upwork': 'briefcase',
        'podio': 'podcast',
        'uber': 'taxi',
        'qiita': 'quora',
        'wunderlist': 'book',
        'shopify': 'shopping-cart',
    }.get(name, name)

################################################################################

@Reactor.wsgi.filtertag('json')
def to_json(value):
    return json.dumps(value, sort_keys=False, indent=4, separators=(',', ': '))

#******************************************************************************

@Reactor.wsgi.filtertag('yaml')
def to_yaml(value):
    return yaml.dump(value, default_flow_style=False)

#******************************************************************************

@Reactor.wsgi.filtertag('slice','slice_by')
def slice_by(value, items):
    return [value[n:n + items] for n in range(0, len(value), items)]

################################################################################

@Reactor.wsgi.filtertag(None,'credentials')
def social_credentials(backend):
    from flask import g

    assoc = g.user.social_auth.get(provider=backend.name)

    resp = {}

    if assoc is not None:
        resp = assoc.extra_data
    
    if 'access_token' in resp:
        resp = resp['access_token']
    
    return resp

#******************************************************************************

ENTITY_GROUP = [
    dict(query=[
        'user','team',
    ], style=dict(box='primary', uni='md', col='3'), links={
        'home': "/+%(social)s/+%(alias)s",
    }),
    dict(query=[
        'page','brand',
    ], style=dict(box='info', uni='md', col='4'), links={
        'home': "/+%(social)s/+%(alias)s",
    }),
]

@Reactor.wsgi.filtertag(None,'entities')
def social_entities(backend):
    resp = []

    pth = RPATH('ronin','kyodo',backend.name,'entities.yaml')

    if os.path.exists(pth):
        for key,cfg in yaml.load(open(pth)).iteritems():
            item = dict(social=backend.name, alias=key, schema=cfg)

            item['label'] = key.capitalize()
            item['links'] = {}
            item['style'] = dict(box='danger', uni='md', col='4')

            for profile in ENTITY_GROUP:
                if item['alias'] in profile['query']:
                    for x in profile['links']:
                        item['links'][x] = profile['links'][x] % item

                    item['style'].update(profile['style'])

            resp.append(item)

    return resp

################################################################################

@Reactor.wsgi.filtertag(None,'plugged')
def plugged_backends(backends):
    resp = []

    for name,backend in backends:
        pass

    return filter_backends(
        backends,
        lambda name, backend: (name not in LEGACY_NAMES)
    )

#******************************************************************************

@Reactor.wsgi.filtertag(None)
def get_backend(backends,name):
    for item in backends.items():
        if item[0]==name:
            return item[1]

    return None

#******************************************************************************

@Reactor.wsgi.filtertag(None,'social')
def social_backends(backends):
    return filter_backends(
        backends,
        lambda name, backend: (name not in LEGACY_NAMES)
    )

#******************************************************************************

@Reactor.wsgi.filtertag(None,'legacy')
def legacy_backends(backends):
    return filter_backends(
        backends,
        lambda name, backend: (name in LEGACY_NAMES)
    )

#******************************************************************************

@Reactor.wsgi.filtertag(None,'oauth')
def oauth_backends(backends):
    return filter_backends(
        backends,
        lambda name, backend: issubclass(backend, OAuthAuth)
    )

#******************************************************************************

def filter_backends(backends, filter_func):
    backends = [item for item in backends.items() if filter_func(*item)]
    backends.sort(key=lambda backend: backend[0])
    return backends

