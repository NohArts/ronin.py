from ronin.kata.helpers import *

from .aspects import *

#******************************************************************************

from flask_apscheduler import APScheduler

from ronin.kata.bunkai import Admin as FlaskAdmin

from ronin.kata.bunkai import BaseView, expose
from ronin.kata.bunkai.model.backends.sqlalchemy  import ModelAdmin as BaseModelAdmin
from ronin.kata.bunkai.model.backends.mongoengine import ModelAdmin as BaseDocumentAdmin

################################################################################

@Reactor.extend('wsgi')
class WSGI_Helper(Reactor.Extension):
    expose = expose

    def prepare(self):
        # App
        self.app = Flask(
            __name__,
            static_folder   = RPATH('workdir','cdn'),
            template_folder = RPATH('workdir','tpl','bushido'),
        )

        import ronin.bushido.data

        #******************************************************************************

        from ghost import configure as stt

        self.app.config.from_object(stt)

        try:
            self.app.config.from_object('ronin.kata.local_settings')
        except ImportError:
            for key in dir(stt):
                if key==key.upper():
                    self.app.config[key] = getattr(stt, key)

                #if key not in [
                #    'environ','dirname','abspath','join','urlparse',
                #    'mongourl','mongolink','item',
                #]:

        #******************************************************************************

        for nrw in ['pre_flask','post_flask']:
            for key in ['data']:
                ext = getattr(Reactor, key, None)

                if ext is not None:
                    hnd = getattr(ext, nrw, None)

                    if callable(hnd):
                        hnd(self.app)

                        #delattr(ext, nrw)

        #******************************************************************************

        self.idp = LoginManager()
        self.idp.login_view = 'social_home' # 'main'
        self.idp.login_message = ''
        self.idp.init_app(self.app)

        #******************************************************************************

        self.api = CustomAPI(self.app)

        #self.app.after_request(self.api.renderer)

        #******************************************************************************

        from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore

        self.app.config['SCHEDULER_JOBSTORES'] = {
            'default': SQLAlchemyJobStore(url=self.app.config['SQLALCHEMY_DATABASE_URI'])
        }

        self.clk = APScheduler()
        self.clk.init_app(self.app)
        self.clk.start()

        #******************************************************************************

        self.adm = FlaskAdmin(self.app)

        #self.rpc = FlaskRPQ(self.app)

    ############################################################################

    def lunch(self, *args, **kwargs):
        #@self.app.errorhandler(TemplateAssertionError)
        def error_template(ex):
            return Reactor.wsgi.WebPage('error', 'syntax', {
                'exception': ex,
            }, title="Welcome to the IT-Tohoku").render(), 500

        #@self.app.errorhandler(500)
        def error_internal(ex):
            return Reactor.wsgi.WebPage('error', 'internal', {
                'exception': ex,
            }, title="Welcome to the IT-Tohoku").render(), 500

        #@self.app.errorhandler(404)
        def error_missing(ex):
            return Reactor.wsgi.WebPage('error', 'missing', {
                'exception': ex,
            }, title="Welcome to the IT-Tohoku").render(), 404

        #******************************************************************************

        print "Registering Social blueprint ..."

        self.app.register_blueprint(social_auth)

        init_social(self.app, Reactor.data.nosql)

    ############################################################################

    def extend(self, *args, **kwargs):
        instance = None

        def do_apply(callback,alias,narrow,target,**options):
            nspace = __import__(narrow)

            if nspace is not None:
                handler = getattr(nspace,target,None)

                if callable(handler):
                    instance = handler(self.app,**options)

                    if alias is not None:
                        setattr(self, alias, instance)

                    if callable(callback):
                        callback(self.app, instance)

            return callback

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #******************************************************************************

    def filtertag(self, *args, **kwargs):
        def do_apply(handler,*target,**options):
            if callable(handler):
                for alias in target:
                    if alias is None:
                        alias = handler.__name__

                    self.app.jinja_env.filters[alias] = handler

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #******************************************************************************

    def globaltag(self, *args, **kwargs):
        def do_apply(handler,*target,**options):
            if callable(handler):
                for alias in target:
                    if alias is None:
                        alias = handler.__name__

                    self.app.jinja_env.globals[alias] = handler

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #******************************************************************************

    def templatetag(self, *args, **kwargs):
        def do_apply(handler,*target,**options):
            if callable(handler):
                for alias in target:
                    if alias is None:
                        alias = handler.__name__

                    #self.app.jinja_env.globals[alias] = handler

                    raise NotImplemented()

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #******************************************************************************

    def register(self, *args, **kwargs):
        def do_apply(handler,pattern,**options):
            if isclass(handler):
                if issubclass(handler,self.Explorer):
                    resp = handler(handler.PATH,pattern,**options)

                    self.adm.add_view(resp)

                elif issubclass(handler,self.Manager):
                    resp = handler(pattern,**options)

                    self.adm.add_view(resp)

                elif issubclass(handler,self.Resource):
                    self.api.add_resource(handler, pattern)

            elif callable(handler):
                self.app.route(pattern)(handler)

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #####################################################################

    class Explorer(BaseExplorer):
        allowed_extensions = ('swf', 'jpg', 'gif', 'png')

        can_upload = True
        can_delete = True
        can_delete_dirs = True
        can_mkdir = True
        can_rename = True

        list_template = 'admin/file/list.html'
        upload_template = 'admin/file/form.html'
        mkdir_template = 'admin/file/form.html'
        rename_template = 'admin/file/rename.html'

    #####################################################################

    class ModelAdmin(BaseModelAdmin):
        pass

    #********************************************************************

    class DocumentAdmin(BaseDocumentAdmin):
        pass

    #####################################################################

    class Manager(BaseView):
        pass

    #####################################################################

    class Resource(BaseResource):
        pass

    #####################################################################

    class WebPage(BaseLayout):
        def setup(self, **kwargs):
            for key in ['title']:
                if key in kwargs:
                    setattr(self, key, kwargs[key])

                    del kwargs[key]

        @property
        def template(self):
            if self.parent is None:
                return 'pages/%s.html' % self.narrow
            elif self.parent in ('special','health','tools','error'):
                return '%s/%s.html' % (self.parent,self.narrow)
            else:
                return 'section/%s/%s.html' % (self.parent,self.narrow)

        @property
        def topmenu(self):
            return [{
                'name': "Domain", 'list': [{
                    'name': "Accounts", 'link': '/auth/'
                },{
                    'name': "Datasets", 'link': '/data/'
                }]
            },{
                'name': "Organism", 'list': [{
                    'name': "Bookmarks", 'link': '/mark/'
                },{
                    'name': "Cables", 'link': '/feed/'
                },{
                    'name': "Disks", 'link': '/disk/'
                }]
            },{
                'name': "Personal", 'list': [{
                    'name': "iCal/vCard", 'link': '/ical/'
                },{
                    'name': "Inbox", 'link': '/mail/'
                },{
                    'name': "Tracker", 'link': '/task/'
                }]
            }]

