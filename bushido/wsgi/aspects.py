from ronin.kata.helpers import *

from ghost import configure as stt

from ghost.configure import environ, BASE_DIR, RPATH

#******************************************************************************

from flask_restful import Resource as BaseResource
from flask_restful import Api      as BaseApi

################################################################################

class CustomAPI(BaseApi):
    def __init__(self, *args, **kwargs):
        super(CustomAPI, self).__init__(*args, **kwargs)
        
        self.aspectify = {
            'text': [
                ('txt',   ['text/plain']),
                ('md',    ['text/markdown']),
                ('html',  ['text/html']),
            ],
            'data': [
                ('latex', ['application/x-latex','text/x-latex']),
                ('yaml',  ['application/yaml','text/yaml']),
                ('df',    ['application/pandas']),

                ('dbf',   ['application/dbf']),
                ('tsv',   ['application/tsv']),
                ('csv',   ['application/csv']),
                ('json',  ['application/json']),
                ('xml',   ['application/xml']),

                ('ods',   ['application/vnd.oasis.opendocument.spreadsheet']),
                ('xls',   ['application/vnd.ms-excel',
'application/msexcel','application/x-msexcel','application/x-ms-excel',
'application/x-excel','application/x-dos_ms_excel',
'application/xls','application/x-xls',
                ]),
                ('xlsx',  ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']),
            ],
            'link': [
                ('nt',    ['text/nt']),
                ('n3',    ['text/n3']),
                ('nquad', ['text/nquads']),
                ('ttl',   ['text/turtle']),
                ('rdf',   ['application/xml+rdf','application/xml+rdfs']),
            ],
        }

        self.representations = {}

        for nrw in self.aspectify:
            hnd = getattr(self, 'output_%s' % nrw, None)

            if callable(hnd):
                for key,lst in self.aspectify[nrw]:
                    for mime in lst:
                        self.representations[mime] = hnd

    def output_file(self, data, code, path, headers=None):
        resp = make_response(data, code)
        resp.headers.extend(headers or {})
        headers["Content-Disposition"] = "attachment; filename=%s" % path
        return resp

    def output_text(self, data, code, headers=None):
        resp = self.renderer(data)

        if type(resp) is Response:
            return resp

        elif type(resp) is Reactor.wsgi.WebPage:
            kwargs = {}

            if type(resp.content) is dict:
                kwargs.update(resp.content)

            resp = resp.render(**kwargs)

        elif type(resp) in (str,unicode):
            return make_response(resp, code)

        else:
            if resp is not None:
                return resp
            else:
                return data

    #####################################################################

    def render_data(self, page, data, code, headers=None):
        for mode,mime in self.aspectify[data]:
            if headers['Content-Type'] in mime:
                resp = data.export(mode)

                return self.output_file(resp,code,'export.%s' % mode,headers)

        return None

    def render_link(self, page, data, code, headers=None):
        for mode,mime in self.aspectify['link']:
            if headers['Content-Type'] in mime:
                resp = data.serialize(format=mode)

                return self.output_file(resp,code,'export.%s' % mode,headers)

        return None

    #####################################################################

    def renderer(self, data):
        if type(data) is dict:
            if 'message' in data:
                data = Reactor.wsgi.WebPage('error', 'generic', data, title="Exception occured")

        resp = None

        if type(data) is Reactor.wsgi.WebPage:
            for alias,handle in [
                ('tabular',   Reactor.wsgi.api.render_data),
                #('datasets',  Reactor.wsgi.render_arch),
                ('knowledge', Reactor.wsgi.api.render_link),
            ]:
                raw = getattr(data,alias,None)

                if raw is not None:
                    resp = handle(data, raw, code, headers)

            if resp is None:
                kwargs = {}

                if type(data.content) is dict:
                    kwargs.update(data.content)

                resp = data.render(**kwargs)
        elif type(data) is Response:
            return data
        else:
            print "Unkown type '%s' for response : %s" % (type(data),data)

            if data is not None:
                return data
            else:
                raise NotImplemented()

################################################################################

class BaseRender(Atomic):
    def initialize(self, *args, **kwargs):
        self.trigger('prepare', *args, **kwargs)

    def render(self, **context):
        context['page'] = self

        return render_template(self.template, **context)

################################################################################

class BaseLayout(BaseRender):
    def prepare(self, parent, narrow, content, **kwargs):
        self._prn = parent
        self._nrw = narrow
        self._raw = content

        self.trigger('setup', **kwargs)

    parent = property(lambda self: self._prn)
    narrow = property(lambda self: self._nrw)

    content = property(lambda self: self._raw)

    @property
    def mime_mode(self):
        if not hasattr(self, '_mym'):
            mode = 'auto'

            setattr(self,'_mym',mode)

        return self._mym

    NUMERIC_TYPEs = (
        int,
    )
    STRING_TYPEs = (
        int,
    )
    GROUP_TYPEs = (
        tuple,set,frozenset,list
    )
    MAPPING_TYPEs = (
        dict,OrderedDict
    )
    STD_TYPEs = NUMERIC_TYPEs + STRING_TYPEs + GROUP_TYPEs + MAPPING_TYPEs

    @property
    def tabular(self):
        resp = None

        if type(self.content) in self.MAPPING_TYPEs:
            for key in ('listing','records','tabular'):
                if key in self.content:
                    resp = self.content[key]

        if type(resp) is records.RecordCollection:
            resp = resp.dataset

        if type(resp) is tablib.Dataset:
            return resp

        return None

    @property
    def datasets(self):
        resp = None

        if type(self.content) in self.MAPPING_TYPEs:
            for key in ('archive','dataset'):
                if key in self.content:
                    resp = self.content[key]

        if type(resp) is records.RecordCollection:
            resp = resp.dataset

        if type(resp) in [tablib.Databook]:
            return resp

        return None

    @property
    def knowledge(self):
        resp = None

        if type(self.content) in self.MAPPING_TYPEs:
            for key in ('knows','facts','ontology'):
                if key in self.content:
                    resp = self.content[key]

        #if type(resp) is owlready.Classe:
        #    resp = None # resp.dataset

        if type(resp) is rdflib.URIRef:
            resp = None # resp.dataset

        if type(resp) in (
            rdflib.Graph, rdflib.ConjunctiveGraph,
        ):
            return resp

        return None

################################################################################

from ronin.kata.bunkai.contrib.fileadmin import FileAdmin

class BaseExplorer(FileAdmin):
    allowed_extensions = ('swf', 'jpg', 'gif', 'png')

    can_upload = True
    can_delete = True
    can_delete_dirs = True
    can_mkdir = True
    can_rename = True

    list_template = 'admin/file/list.html'
    upload_template = 'admin/file/form.html'
    mkdir_template = 'admin/file/form.html'
    rename_template = 'admin/file/rename.html'

