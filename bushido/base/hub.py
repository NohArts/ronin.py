from ronin.abstract.shortcuts import *

#******************************************************************************

#from datafs import DataAPI

##########################################################################

@Reactor.extend('hub')
class Manager(Reactor.Extension):
    def prepare(self):
        self.title = None
        self.space = None

        self.descr = ""
        self.words = []
        self.cover = ""

        self.build = ['python','nodejs']
        self._vars = {}
        self.setup = None

        self.setup = ['heroku-postgresql']
        self.lands = '/'

        self._net = {}
        self._cfg = {}
        self._env = {}

        self._web = {}
        self._wrk = []

        self._plg = []
        self._app = []

    #********************************************************************

    network = property(lambda self: self._net)
    config  = property(lambda self: self._cfg)
    environ = property(lambda self: self._env)

    routing = property(lambda self: self._web)
    workers = property(lambda self: self._wrk)

    plugins = property(lambda self: self._plg)
    applets = property(lambda self: self._app)

    #********************************************************************

    def plugin(self, realm, scope, *targets, **options):
        for target in targets:
            self._plg.append({ "type" : "class",
                "realm" : realm,
                "role" : scope,
            "classname" : target })

        return self

    def applet(self, realm, scope, *targets, **options):
        for target in targets:
            self._app.append({ "type" : "class",
                "realm" : realm,
                "role" : scope,
            "classname" : target })

        return self

    #********************************************************************

    def routes(self, alias, target):
        if alias not in self.routing:
            self._web[alias] = target

        return self

    def folder(self, alias, **mapping):
        return self.routes(alias, {
            "type" : "path",
            "paths" : mapping,
        })

    #####################################################################

    def static(self, path, listing=False, mime={}):
        return dict(type='static', directory=path, options={
            "mime_types" : {
                ".svg" : "image/svg+xml"
            },
        })

    #********************************************************************

    def wsgi(self, nspace, target="app"):
        return dict(type='wsgi', module=nspace, object=target)

    #********************************************************************

    def proxy(self, path, port, host='localhost'):
        return dict(type='reverseproxy', path=path, port=port, host=host)

    #********************************************************************

    def hook(self, path, listing=False, mime={}):
        return dict(type='static', directory=rpath, options={
            "mime_types" : {
                ".svg" : "image/svg+xml"
            },
        })

    #####################################################################

    def daemon(self, prog, args=[], curr='.', env={}, watch=None):
        item = dict(type='guest', executable=prog, arguments=args, options={
            "workdir" : curr,
            "env" : {
               "inherit" : True,
               "vars" : env
            }
        })

        if type(watch) in (tuple,set,list):
            pass # item['options']['watch'] = {}

        self._wrk.append(item)

        return self

    def server(self, path, port, addr, *args, **kwargs):
        self.routes(path, self.proxy(addr,port))

        return self.daemon(*args, **kwargs)

    def backend(self, path, port, addr, *args, **kwargs):
        self.routes(path, self.proxy(addr,port))

        return self.daemon(*args, **kwargs)

    #####################################################################

    def command(self, acte):
        if acte in ('stop','status'):
            os.system('crossbar %s' % acte)

        elif acte=='start':
            os.system('crossbar start --cbdir . --config crossed.json')

        else:
            raise NotImplemented('crossbar %s' % acte)

    #********************************************************************

    def persist(self):
        path = self.routing

        path['poll'] = { "type" : "longpoll" }

        mqtt = dict(realm=self.space.alias, scope='anonymous', route={
            "" : {
                "decoder" : "com.example.mqtt.decode",
                "realm" : self.space.alias,
                "type" : "dynamic",
                "encoder" : "com.example.mqtt.encode"
            },
        })

        sock = dict(
            serial=["cbor","msgpack","ubjson","json"],
            author="%s.authentify" % self.space.alias,
        )

        resp = {
            "version" : 2,
            "controller" : {},
            "workers" : [{
                "type" : "router",
                "connections" : [],
                "options" : {
                    "title" : self.title,
                    "reactor" : {
                        "linux" : "epoll"
                    },
                    "cpu_affinity" : [ 0 ],
                    "env" : self.environ,
                    "pythonpath" : []
                },
                "transports" : [{
                    "type" : "universal",
                    "endpoint" : { "type" : "tcp", "port" : "$PORT" },
                    "web" : { "paths" : path },
                    "mqtt" : {
                        "options" : {
                            "role" : mqtt['scope'],
                            "payload_mapping" : mqtt['route'],
                            "realm" : mqtt['realm']
                        }
                    },
                    "websocket" : {
                        "sock" : {
                            "type" : "websocket",
                            "serializers" : sock['serial'],
                            "auth" : {
                                "ticket" : {
                                    "authenticator" : sock['author'],
                                    "type" : "dynamic"
                                }
                            }
                        }
                    },
                    "rawsocket" : { "serializers" : sock['serial'] }
                }],
                "components" : self._app,
                "realms" : [self.space.__json__]
            }] + self.workers
        }

        #if os.path.exists('config.json'):
        #    os.system('mv config.json backup-%s.json' % datetime.now())

        write_json(resp, 'config.json')

    #####################################################################

    def param(self, key, mandatory=False, value='', summary=None):
        if summary is None:
            summary = "Help for %s." % key

        self._vars[key] = {
            'value': value,
            'required': mandatory,
            'description': summary,
        }

    #********************************************************************

    def compiler(self, *path, **context):
        target = rpath(*path)

        if os.path.exists(target):
            with open(target) as src:
                exec src.read() in globals(),context

    #********************************************************************

    def loader(self, mode, *params, **context):
        self.compiler('ghost','provision.py')

        if mode=='dojo':
            self.compiler('ghost','line','%(GHOST_MODULE)s.py' % os.environ)

        elif mode=='na':
            self.compiler('ghost','line','%(GHOST_MODULE)s.py' % os.environ)

        elif mode=='ryu':
            pass

    #####################################################################

    class Realm(Atomic):
        def initialize(self, alias, *roles):
            self._key = alias
            self._acl = {}

            self._eve = [{
                "match" : "exact", "limit" : 1000,
                "uri" : "%s.logging" % self.alias,
            }]
            self._que = [{
                "match" : "exact", "limit" : 1000,
                "uri" : "%s.logging" % self.alias,
            }]

        alias = property(lambda self: self._key)
        roles = property(lambda self: self._acl)

        event = property(lambda self: self._eve)
        queue = property(lambda self: self._que)

        def scope(self, alias, *args, **kwargs):
            obj = None

            if alias in self._acl:
                obj = self._acl[alias]
            else:
                obj = Reactor.hub.Scope(self, alias, *args, **kwargs)

                self._acl[alias] = obj

            return obj

        @property
        def __json__(self):
            return {
               "name" : self.alias,
               #"store" : {
               #   "event-history" : self.event,
               #   "call-queue" : self.queue,
               #},
               "roles" : [x.__json__ for x in self._acl.values()],
               "options" : {
                  "uri_check" : "strict"
               }
         }

    #********************************************************************

    class Scope(Atomic):
        def initialize(self, realm, alias, *permissions):
            self._prn = realm
            self._key = alias
            self._acl = []

        realm = property(lambda self: self._prn)
        alias = property(lambda self: self._key)
        perms = property(lambda self: self._acl)

        def add(self, resolv, target, call=False,publish=False,subscribe=False,register=False):
            self._acl.append({
                "match" : resolv,
                "uri" : target,
                "allow" : {
                    "call" : call,
                    "publish" : publish,
                    "subscribe" : subscribe,
                    "register" : register
                }
            })

            return self

        def prefix(self, *args, **kwargs): return self.add('prefix', *args, **kwargs)
        def exact(self, *args, **kwargs):  return self.add('exact', *args, **kwargs)
        def suffix(self, *args, **kwargs): return self.add('suffix', *args, **kwargs)

        @property
        def __json__(self):
            return {
                 "name" : self.alias,
                 "permissions" : self._acl
            }

    #####################################################################

    class Component(Atomic):
        def initialize(self, realm, namespace, **permissions):
            self._prn = realm
            self._key = alias
            self._acl = {}

        realm = property(lambda self: self._prn)
        alias = property(lambda self: self._key)
        roles = property(lambda self: self._acl)

        @property
        def __json__(self):
            return {
            }

    #********************************************************************

    class Worker(Atomic):
        def initialize(self, program, *arguments, **options):
            self._prn = realm
            self._key = alias
            self._acl = {}

        realm = property(lambda self: self._prn)
        alias = property(lambda self: self._key)
        roles = property(lambda self: self._acl)

        @property
        def __json__(self):
            return {
                "arguments" : [
                    "-i",
                    "30",
                    "-u",
                    "$FOCUS_URL"
                ],
                "options" : {
                    "workdir" : ".",
                    "env" : {
                        "inherit" : True,
                        "vars" : {
                        }
                    }
                },
                "type" : "guest",
                "executable" : "rqscheduler"
            }

