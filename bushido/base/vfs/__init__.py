from ronin.abstract.shortcuts import *

#******************************************************************************

from .abstract import *
from .helpers import *

##########################################################################



##########################################################################

@Reactor.extend('vfs')
class Manager(Reactor.Extension):
    def prepare(self):
        self._vfs = {}

        #self._api = DataAPI(
        #     username='My Name',
        #     contact = 'my.email@example.com')

        #self._mgr = MongoDBManager(
        #    database_name = 'MyDatabase',
        #    table_name = 'DataFiles')

        #self._mgr.create_archive_table('DataFiles')

        #self._api.attach_manager(self._mgr)

    #********************************************************************

    volumes = property(lambda self: self._vfs)
    dataAPI = property(lambda self: self._api)
    manager = property(lambda self: self._mgr)

    def append(self, alias, target, attach=False):
        if alias not in self._vfs:
            self._vfs[alias] = target

        if attach:
            self._api.attach_authority(alias, target)

        return target

    def __getitem__(self, key):
        if key not in self._vfs:
            return self._vfs[key]

        return OSFS('.')

    #********************************************************************

    def console(self, alias, *target):
        resp = OSFS('/'.join(target))

        return self.Volume(resp)

    def combine(self, alias, **sources):
        from fs.mountfs import MountFS

        resp = MountFS()

        for key,obj in sources.iteritems():
            resp.add_fs(key,obj)

        return self.Volume(resp)

    def compose(self, alias, **mapping):
        from fs.multifs import MultiFS

        resp = MultiFS()

        for key,obj in mapping.iteritems():
            resp.mount(key,obj)

        return self.Volume(resp)

    #********************************************************************

    def remote(self, alias, target, *args, **kwargs):
        from fs.rpcfs import RPCFS

        resp = RPCFS(target)

        return self.Volume(resp)

    def server(self, alias, target, *args, **kwargs):
        from fs.sftpfs import SFTPFS

        resp = SFTPFS(connection, root_path='/',
            encoding=None,
            hostkey=None,
                username='',
                password=None,
            pkey=None, agent_auth=True, no_auth=False,
        look_for_keys=True)

        return self.Volume(resp)

    def webdav(self, alias, target, *args, **kwargs):
        from fs.contrib.davfs import DAVFS

        resp = DAVFS(url, dict(
            username = 'xxx',
            password = 'yyy',
        ), thread_synchronize=True, connection_classes=None, timeout=None)

        return self.Volume(resp)

    def bucket(self, alias, target, **kwargs):
        from fs.s3fs import S3FS

        resp = S3FS(target, prefix='',
            aws_access_key=None,
            aws_secret_key=None,
        separator='/', thread_synchronize=True, key_sync_timeout=1, **conn_kwargs)

        return self.Volume(resp)

    #********************************************************************

    def resolve(self, link, **opts):
        uri = urlparse(link)

        if uri.scheme=='debug':
            return 
        elif uri.scheme in ('temp','tempfs'):
            return 
        elif uri.scheme in ('ram','mem','memory'):
            return 
        elif uri.scheme in ('file','osfs'):
            return 
        elif uri.scheme in ('ftp','sftp'):
            return 
        elif uri.scheme in ('http','https'):
            return 
        elif uri.scheme in ('dav','webdav'):
            return 
        elif uri.scheme in ('s3','s3fs','minio'):
            return 
        elif uri.scheme in ('boxnet','dropbox'):
            return 
        elif uri.scheme in ('cifs','smbfs','smb','samba'):
            return 
        else:
            print "Unsupported FS : %s" % link

        return None

    #********************************************************************

    class Mapper(BaseVFS):
        def prepare(self, *args, **kwargs):
            pass

    #********************************************************************

    class Volume(BaseVFS):
        def prepare(self, *args, **kwargs):
            pass

    #********************************************************************

    class Physic(BaseVFS):
        def prepare(self, *args, **kwargs):
            pass

    #********************************************************************

    class Folder(BaseVFS):
        def prepare(self, rpath, *args, **kwargs):
            self._pth = rpath

            self._vfs = SubFS(self.rootFS, rpath)

            #self.trigger('prepare', *args, **kwargs)

