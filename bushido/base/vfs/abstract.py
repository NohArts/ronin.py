from ronin.abstract.shortcuts import *

#******************************************************************************

#from datafs.managers.manager_mongo import MongoDBManager
#from datafs import DataAPI

##########################################################################

class BaseVFS(Atomic):
    def initialize(self, root_fs, *args, **kwargs):
        self._vfs = root_fs or self.DEFAULT

        self.trigger('prepare', *args, **kwargs)

    rootFS = property(lambda self: self._vfs)

    #********************************************************************

    def __getitem__(self, path):
        return Reactor.disk.Folder(self.rootFS, path)

    def __call__(self, program, *args, **kwargs):
        pass

    #********************************************************************

    def expose(self, mode, port=8080, addr=''):
        srv = None

        if mode=='rpc':
            from fs.expose.xmlrpc import RPCFSServer

            srv = RPCFSServer(ctx.obj['vfs'],(addr,port))

        elif mode=='http':
            pass

        elif mode=='sftp':
            from fs.expose.sftp import SFTPServer

            srv = SFTPServer((addr,port),ctx.obj['vfs'])

        if srv is not None:
            srv.serve_forever()

