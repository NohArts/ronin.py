from ronin.abstract.shortcuts import *

#******************************************************************************

#from datafs import DataAPI

##########################################################################

@Reactor.extend('pkg')
class Manager(Reactor.Extension):
    def prepare(self):
        self._lst = []

        self._mgr = {}

    #********************************************************************

    jobs = property(lambda self: self._vfs)
    apis = property(lambda self: self._api)

    #********************************************************************

    def persist(self):
        for entry in Reactor.pkg.artefacts:
            entry['finale'] = entry.get('finale','') or entry['tmpkey']

            if entry['type']=='archive':
                if not os.path.exists(entry['target']):
                    os.system('wget -c %(source)s')

                    if extens in ('tgz','tar.gz'):
                        os.system('tar zxf %(tmpkey)s.%(extens)s')

                    os.system('mv %(finale)s %(target)s')

            elif entry['type']=='file':
                if not os.path.exists(entry['target']):
                    os.system('wget -c %(source)s')

                    os.system('mv %(finale)s %(target)s')

    #####################################################################

    def package(self, *args, **kwargs):
        def do_walks(alias,version, *depends):
            yield alias,version

            for alias in depends:
                version = None

                if '==' in alias:
                    alias,version = alias.split('==',1)

                for op in ('>=','>','<=','<','~','^'):
                    if version is None:
                        if op in alias:
                            alias,version = alias.split(op,1)
                            version = op + version

                yield alias,version

        def do_apply(callback, manager, alias, version=None, *depends):
            if manager not in self._dep:
                self._dep[manager] = {}

            for k,v in do_walks(alias,version, *depends):
                if k not in self._dep[manager]:
                    self._dep[manager][k] = v

            if callable(callback):
                callback()

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #********************************************************************

    def binary(self, *args, **kwargs):
        def do_apply(callback, target):
            pass

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #********************************************************************

    def archive(self, *args, **kwargs):
        def do_apply(callback, target):
            pass

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #####################################################################

    def manifest(self, lang):
        resp = {
            "name": Reactor.hub.title,
            "description": Reactor.hub.descr,
            "keywords": Reactor.hub.words + ["neurochip"],
            "logo": Reactor.hub.cover,
            #"repository": "https://github.com/cdubz/babybuddy",
            #"version": "0.1.0",
            #"license": "MIT",
            #"author": "TAYAA Med Amine",
            #"website": "https://github.com/neurochip/asgarth",
        }

        if lang is None:
            resp.update({
                "buildpacks": [dict(url='heroku/'+x) for x in Reactor.hub.build],
                "scripts": {
                    "postdeploy": "python -m ronin.kata.dojo deploy"
                },
                "success_url": Reactor.hub.lands,
                "env": {
                    "GHOST_TARGET": { "description": "Target endpoint of Ronin specs", "required": True, "value": "tohoku.herokuapp.com" },
                    "GHOST_PERSON": { "description": "Target person of the Ronin specs", "required": True, "value": "tayamino" },
                    "GHOST_REALMS": { "description": "List of realms for the Ronin specs", "required": True, "value": "tohoku" },
                    "GHOST_CYPHER": { "description": "Access token of the Ronin specs", "required": True, "value": "b7edaa2dcd338e71584c9fb929b6b384024012493ad085" },

                    "GHOST_MODULE": { "description": "Sub-module used in this new app", "required": True, "value": "asgarth" },
                    "GHOST_DOMAIN": { "description": "Domain name of this new app", "required": True, "value": "akasha.herokuapp.com" },
                    "GHOST_SECRET": { "description": "Secret key for to be used by this instance (for : sessions, cookies, cypher, ...)", "generator": "secret" }
                },
                "addons": ["heroku-postgresql"]
            })

            for key in Reactor.hub._vars:
                resp['env'][key] = Reactor.hub._vars[key]

            write_json(resp, 'app.json')

        elif lang=='nodejs':
            resp.update({
                "scripts": {
                    "start": "electron ."
                },
                "main": "program/atom.js",
                "bin": {
                    "me-api-server": "./bin/www",
                    "me-api-init": "./bin/init"
                },
                "dependencies": {}
            })

            write_json(resp, 'package.json')

        elif lang=='python':
            pass

    #####################################################################

    class Dependency(Atomic):
        pass

    #********************************************************************

    class Manager(Atomic):
        pass

    #####################################################################

    class Project(Atomic):
        pass

    #********************************************************************

    class Platform(Atomic):
        pass

