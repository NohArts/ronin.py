#!/usr/bin/env python

import os, sys, re, click, git

#***************************************************************************************

def shell(prog, *args):
    stmt = ' '.join([(x if ' ' not in x else '"%s"' % x) for x in args])

    os.system('%s %s' % (prog, stmt))

#***************************************************************************************

def list_dir(path):
    if os.path.isdir(path):
        for key in os.listdir(path):
            trg = os.path.join(path,key)

            if os.path.isdir(trg):
                yield key,trg

########################################################################################

TARGET_PATH  = os.environ.get('DEMING_PATH',os.environ['PWD'])

#***************************************************************************************

def handle_missing_dir(ctx, *args, **kwargs):
    print ctx, dir(ctx)

    exit()

#***************************************************************************************

@click.group()
@click.option('--path', default=os.getcwd())
@click.option('--verbose/--quiet','-v/-q', default=False)
@click.pass_context
def cli(ctx, *args, **kwargs):
    target = kwargs['path']

    if os.path.exists(target):
        if kwargs['verbose']:
            print "Deming running against : %s" % target

        ctx.obj.update({
            'verb': kwargs['verbose'],
            'path': os.path.abspath(target),
            'repo': None,
        })

        os.chdir(ctx.obj['path'])

        try:
            ctx.obj['repo'] = git.Repo(ctx.obj['path'])
        except:
            if kwargs['verbose']:
                print "Supplied path is not a Repository at : %s" % ctx.obj['path']

            handle_missing_dir(ctx, *args, **kwargs)
    else:
        if kwargs['verbose']:
            print "Supplied path does'nt exist at : %s" % target

        handle_missing_dir(ctx, *args, **kwargs)

#***************************************************************************************

def git_walking(path):
    if os.path.exists(path):
        if os.path.isdir(path):
            try:
                obj = git.Repo(path)

                yield path
            except:
                for folder in list_dir(path):
                    for target in git_walking(path):
                        yield target

def git_is_repo(path):
    if os.path.exists(path):
        if os.path.isdir(path):
            try:
                obj = git.Repo(path)

                return True
            except:
                pass

    return False

#***************************************************************************************

def git_discover_normal(ctx, *args, **kwargs):
    if kwargs['system']:
        if git_is_repo('/etc'):
            yield 'system','etc','/etc'

    if kwargs['language']:
        pass

    if kwargs['realm']:
        pass

    if kwargs['feature']:
        pass

#***************************************************************************************

def git_discover_ronin(ctx, *args, **kwargs):
    for nrw,sub in [
        ('system',   ['sys']),
        ('language', ['lib']),
        ('realm',    ['opt']),
        ('feature',  ['dev']),
    ]:
        target = os.path.join(os.environ['RONIN_ROOT'], *sub)

        for key,pth in list_dir(target):
            if git_is_repo(pth):
                yield nrw,key,pth

#***************************************************************************************

def git_discover_codes(ctx, *args, **kwargs):
    for nrw,sub in [
        ('system',   ['sys']),
        ('language', ['lib']),
        ('realm',    ['opt']),
        ('feature',  ['dev']),
    ]:
        target = os.path.join(os.environ['RONIN_ROOT'], *sub)

        for key,pth in list_dir(target):
            if git_is_repo(pth):
                yield nrw,key,pth

#***************************************************************************************

@cli.command('discover')
@click.option('--mode','-m', default='auto')
@click.option('--system/--no-system','-s', default=True)
@click.option('--language/--no-language','-l', default=True)
@click.option('--realm/--no-realm','-r', default=True)
@click.option('--feature/--no-feature','-f', default=True)
@click.option('--all/--dirty','-a/-d', default=True)
@click.pass_context
def git_discover(ctx, *args, **kwargs):
    if kwargs['mode']=='auto':
        if 'RONIN_ROOT' in os.environ:
            kwargs['mode'] = 'ronin'
        else:
            kwargs['mode'] = 'normal'

    handler = {
        'ronin': git_discover_ronin,
    }.get(kwargs['mode'],git_discover_normal)

    if ctx.obj['verb']:
        print "Discovering repositories in mode : %s" % kwargs['mode']

    for nrw,key,pth in handler(ctx, *args, **kwargs):
        print pth

########################################################################################

@cli.command('status')
@click.option('--summary/--details','-s/-d', default=True)
@click.pass_context
def git_status(ctx, *args, **kwargs):
    if ctx.obj['repo'].is_dirty():
        if kwargs['summary']:
            print "dirty"
        else:
            shell('git','status')
    else:
        if kwargs['summary']:
            print "clean"
        else:
            print "Repository is clean !"

#***************************************************************************************

@cli.command('commit')
@click.option('--add','-a', default=False)
@click.option('--message','-m', default="...")
@click.pass_context
def git_commit(ctx, *args, **kwargs):
    if ctx.obj['repo'].is_dirty():
        if kwargs['add']:
            shell('git','add','--all')

        shell('git','commit','-a','-m',kwargs['message'])
    else:
        print "clean"

########################################################################################

@cli.command('pull')
@click.option('--branch','-b', default='master')
@click.option('--upstream','-u', multiple=True)
@click.pass_context
def git_pull(ctx, *args, **kwargs):
    if len(kwargs['upstream'])==0:
        kwargs['upstream'] = ['origin']

    for remote in kwargs['upstream']:
        shell('git','pull',remote,kwargs['branch'],'-v')

#***************************************************************************************

@cli.command('push')
@click.option('--branch','-b', default='master')
@click.option('--upstream','-u', multiple=True)
@click.pass_context
def git_push(ctx, *args, **kwargs):
    if len(kwargs['upstream'])==0:
        kwargs['upstream'] = ['origin']

    for remote in kwargs['upstream']:
        shell('git','push',remote,kwargs['branch'],'-v')

#***************************************************************************************

@cli.command('sync')
@click.option('--branch','-b', default='master')
@click.option('--upstream','-u', multiple=True)
@click.pass_context
def git_sync(ctx, *args, **kwargs):
    if len(kwargs['upstream'])==0:
        kwargs['upstream'] = ['origin']

    for remote in kwargs['upstream']:
        shell('git','pull',remote,kwargs['branch'],'-v')

    for remote in kwargs['upstream']:
        shell('git','push',remote,kwargs['branch'],'-v')

########################################################################################

if __name__ == '__main__':
    cli(
        obj={
            'debug': True,
            #'key': 'value',
            'python': '/usr/bin/python',
            'pip':    '/usr/bin/pip',
        }
    )

